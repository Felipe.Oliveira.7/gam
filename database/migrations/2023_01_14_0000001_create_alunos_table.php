<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alunos', function (Blueprint $table) {
            $table->id();
            $table->string('nome', 150);
            $table->integer('serie');
            $table->string('escola', 150);
            $table->date('data_nascimento');
            $table->string('endereco', 200);
            $table->integer('numero')->nullable();
            $table->string('bairro', 200);
            $table->string('telefone', 15)->nullable();
            $table->integer('qtd_familiares');
            $table->string('nis', 18)->nullable();
            $table->decimal('renda_familiar');
            $table->string('nome_pai', 150);
            $table->string('telefone_pai', 15)->nullable();
            $table->string('nome_mae', 150);
            $table->string('telefone_mae', 15)->nullable();
            $table->string('local_trabalho_pai', 200)->nullable();
            $table->string('local_trabalho_mae', 200)->nullable();
            $table->date('data_inicio');
            $table->string('projeto', 50)->nullable();
            $table->string('encaminhamento', 50)->nullable();
            $table->date('data_desligamento')->nullable();
            $table->string('motivo_desligamento', 100)->nullable();
            $table->text('observacao')->nullable();
            $table->binary('foto')->nullable();
            $table->string('foto_tipo', 5);
            $table->string('foto_nome', 150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alunos');
    }
};
