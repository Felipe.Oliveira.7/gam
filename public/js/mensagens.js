jQuery.extend(jQuery.validator.messages, {
    required: "Campo obrigatório.",
    remote: "Corrija esse campo.",
    email: "Formato de e-mail inválido.",
    url: "Por favor entre com uma URL válida.",
    date: "Por favor digite um data válida.",
    dateISO: "Por favor digite um formato de data (ISO).",
    number: "Por favor insira um valor numérico válido.",
    digits: "Por favor digite somente letras.",
    creditcard: "Por favor digite um formato válido de cartão de crédito.",
    equalTo: "Por favor entre com o mesmo valor novamente.",
    accept: "Insira um valor com uma extensão válida.",
    maxlength: jQuery.validator.format("Por favor, não insira mais do que {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, não insira menos que {0} caracteres."),
    rangelength: jQuery.validator.format("Insira um valor entre {0} e {1} caracteres."),
    range: jQuery.validator.format("Insira um valor entre {0} e {1}."),
    max: jQuery.validator.format("Insira um valor menor ou igual a {0}."),
    min: jQuery.validator.format("Insira um valor maior ou igual a {0}.")
});
