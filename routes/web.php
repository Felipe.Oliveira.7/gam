<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AlunoController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RelatorioController;
use App\Http\Controllers\OcorrenciaController;

// Rotas de Login
Route::get('/', [UserController::class,'login'])->name('login.page');
Route::post('/auth', [UserController::class, 'auth'])->name('auth.user');
Route::get('/logout', [UserController::class, 'logout'])->name('logout.user');
Route::get('login',array('as'=>'login',function(){ return redirect('/'); }));

// Rotas protegidas
Route::get('/home', [HomeController::class, 'home'])->name('home')->middleware('auth');

// Rotas usuário
Route::get('/usuarios', [UserController::class, 'index'])->name('usuarios.index')->middleware('auth');
Route::post('/usuario', [UserController::class, 'create'])->name('usuario.novo')->middleware('auth');
Route::put('/usuario', [UserController::class, 'update'])->name('usuario.editar')->middleware('auth');
Route::delete('/usuario', [UserController::class, 'delete'])->name('usuario.deletar')->middleware('auth');
Route::get('/usuario', [UserController::class, 'search'])->name('usuario.pesquisar')->middleware('auth');

// Rotas Aluno
Route::get('/aluno', [AlunoController::class, 'new'])->name('aluno.criar')->middleware('auth');
Route::get('/alunos', [AlunoController::class, 'index'])->name('alunos.index')->middleware('auth');
Route::post('/aluno', [AlunoController::class, 'create'])->name('aluno.novo')->middleware('auth');
Route::get('/aluno/{id}/ficha', [AlunoController::class, 'profile'])->name('aluno.ficha')->middleware('auth');
Route::get('/aluno/{id}', [AlunoController::class, 'get'])->name('aluno.perfil')->middleware('auth');
Route::post('/aluno/editar', [AlunoController::class, 'update'])->name('aluno.editar')->middleware('auth');
Route::delete('/aluno', [AlunoController::class, 'delete'])->name('aluno.deletar')->middleware('auth');
Route::put('/aluno/{id}/desligar', [AlunoController::class, 'disconnect'])->name('aluno.desligar')->middleware('auth');

//Rotas Ocorrências
Route::get('/ocorrencias/aluno/{id}', [OcorrenciaController::class, 'get'])->name('ocorrencias.aluno')->middleware('auth');
Route::post('/ocorrencias/aluno/{id}', [OcorrenciaController::class, 'create'])->name('ocorrencia.nova')->middleware('auth');
Route::get('/ocorrencias/aluno/{id}/imprimir', [OcorrenciaController::class, 'events'])->name('ocorrencia.imprimir')->middleware('auth');

// Rotas de Relatório
Route::post('/relatorio/pdf', [RelatorioController::class, 'pdf'])->name('relatorio.pdf')->middleware('auth');
Route::get('/relatorio', [RelatorioController::class, 'index'])->name('relatorios.index')->middleware('auth');
Route::post('/relatorio', [RelatorioController::class, 'get'])->name('relatorio.buscar')->middleware('auth');
