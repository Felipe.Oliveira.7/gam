<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="robots" content="noindex,nofollow,noarchive,nosnippet,noodp,notranslate,noimageindex" />
    <meta name="csrf-token" content="<?php echo csrf_token(); ?>">
    <title>Sistema de Cadastros de Alunos GAM</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
</head>
<style>
    .title{
        margin: 20px;
    }
    .row>*{
        padding-right: 0 !important;
        padding-left: 0 !important;
    }
    .row{
        margin: 3rem !important;
    }
    .mlp-card{
        margin-left: 4rem;
    }
    .top-bar{
        margin: 0 !important;
        padding-right: 1.5rem;
    }
    .min-card{
        min-width: 280px !important;
    }
    .card{
        margin-bottom: 3rem !important;
    }
</style>
<body>
    <div class="container-fluid p-0">
        <div class="row top-bar border-bottom bg-light bg-gradient">
            <div class="col-4">
            </div>
            <div class="col-4 text-center mt-1">
                <a href="/home">
                    <img src="{{URL::asset('imagens/logo.jpg')}}" alt="Logo" class="img-thumbnail" width="52">
                </a>
            </div>
            <div class="col-4 pt-3 pb-3 d-flex flex-row-reverse">
                <a href="/logout" class="btn btn-outline-danger">Sair</a>
            </div>
        </div>

        <div class="row">
            <div class="card w-25 mlp-card min-card">
                <div class="card-header">
                    <div class="d-flex align-items-start">
                        <span class="material-symbols-outlined">
                        person
                        </span>
                        <span>&nbsp;&nbsp;Gerenciamento de Usuários</span>
                    </div>
                </div>
                <div class="card-body">
                    <p class="card-text">Cadastro e edição de usuários que irão acessar o sistema.</p>
                    <a href="{{route('usuarios.index')}}" class="btn btn-primary">Acessar</a>
                </div>
            </div>

            <div class="card w-25 mlp-card min-card">
                <div class="card-header">
                    <div class="d-flex align-items-start">
                        <span class="material-symbols-outlined">
                        supervisor_account
                        </span>
                        <span>&nbsp;&nbsp;Gerenciamento de Alunos</span>
                    </div>
                </div>
                <div class="card-body">
                    <p class="card-text">Cadastro e edição de alunos e responsáveis.</p>
                    <a href="{{route('alunos.index')}}" class="btn btn-primary">Acessar</a>
                </div>
            </div>

            <div class="card w-25 mlp-card min-card">
                <div class="card-header">
                    <div class="d-flex align-items-start">
                        <span class="material-symbols-outlined">
                        clinical_notes
                        </span>
                        <span>&nbsp;&nbsp;Relatórios</span>
                    </div>
                </div>
                <div class="card-body">
                    <p class="card-text">Gerar relatórios de alunos e responsáveis.</p>
                    <a href="{{route('relatorios.index')}}" class="btn btn-primary">Acessar</a>
                </div>
            </div>

        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>

</html>
