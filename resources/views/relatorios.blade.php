<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="robots" content="noindex,nofollow,noarchive,nosnippet,noodp,notranslate,noimageindex" />
    <meta name="csrf-token" content="<?php echo csrf_token(); ?>">
    <title>Sistema de Cadastros de Alunos GAM</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
</head>
<style>
    .title{
        margin: 20px;
    }
    .row>*{
        padding-right: 0 !important;
        padding-left: 0 !important;
    }
    .row{
        margin: 5rem !important;
    }
    .top-bar{
        margin: 0 !important;
        padding-right: 1.5rem;
    }
    .search{
        margin: 1rem 5rem -4rem 5rem;
    }
    .icon-bar{
        font-size: 42px;
    }
    .badge{
        --bs-badge-font-size: 1rem !important;
    }
    .w-column-name{
        max-width: 340px;
    }
    .w-column-schol{
        max-width: 380px;
    }
    .mt-6{
        margin-top: 3.4rem;
    }
    .pl-6{
        padding-left: 1.5rem;
    }
    .pr-6{
        padding-right: 1.5rem;
    }
    .border-relatorio{
       border: solid 1px #dcdcdc !important;
    }
    .btn-actions{
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .btn-right{
        display: flex;
        align-items: right;
        justify-content: right;
    }
    .btn-left{
        display: flex;
        align-items: left;
        justify-content: left;
    }
</style>
<body>
    <div class="container-fluid p-0">

        <div class="row top-bar border-bottom bg-light bg-gradient">
            <nav class="navbar">
                <div class="col-4">
                    <div class="container-fluid">
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>
                </div>
                <div class="col-4 text-center mt-1">
                    <a href="/home">
                        <img src="{{URL::asset('imagens/logo.jpg')}}" alt="Logo" class="img-thumbnail" width="52">
                    </a>
                </div>
                <div class="col-4 pt-3 pb-3 d-flex flex-row-reverse">
                    <a href="/logout" class="btn btn-outline-danger">Sair</a>
                </div>
            </nav>
        </div>

        <div class="collapse" id="navbarToggleExternalContent">
            <div class="bg-secondary p-4">
                <h6 class="text-white"><a href="/home" class="link-light text-decoration-none">Home</a></h6>
                <h6 class="text-white"><a href="/usuarios" class="link-light text-decoration-none">Usuários</a></h6>
                <h6 class="text-white"><a href="/alunos" class="link-light text-decoration-none">Alunos</a></h6>
                <h6 class="text-white">Relatórios</h6>
            </div>
        </div>

        <div class="container-fluid mt-2">
            <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/home">
                    Home
                    </a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    Relatório de Alunos
                </li>
            </ol>
            </nav>
        </div>

        <div class="container-fluid">

            <h5 class="badge bg-light text-dark">
                RELATÓRIO DE ALUNOS
            </h5>
        </div>

        <div class="container bg-light pl-6 pr-6 pb-4 border border-relatorio">
            <form id="formRelatorio" class="g-3" action="{{route('relatorio.buscar')}}" method="POST">
                @csrf
                <div class="form-group input-group rounded">
                    <div class="mt-3 col-12">
                        <h4 class="form-label mt-2"><u>Filtros</u></h4>
                    </div>

                    <div class="mt-3 col-auto">
                        <label for="periodo_de" class="form-label">Período:</label>
                        <input min="1920-01-01" max="{{ now()->toDateString('Y-m-d') }}" type="date" class="form-control" id="periodo_de" name="periodo_de" value="{{$params['periodo_de'] ?? ''}}" onchange="setPeriodoAte(this.value);">
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="mt-6 col-auto">
                        <label for="periodo_ate" class="form-label">até</label>
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="mt-5 col-auto">
                        <input max="{{ now()->toDateString('Y-m-d') }}" type="date" class="form-control" id="periodo_ate" name="periodo_ate" value="{{$params['periodo_ate'] ?? ''}}">
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="mt-3 col-auto">
                        <label for="status" class="form-label">Status:</label>
                        <select class="form-select" aria-label="Selecione um status" id="status" name="status">
                            <option value="" selected>Selecione...</option>
                            <option value="ativo" @isset($params['status']) @if($params['status'] == 'ativo') {{'selected'}} @endif @endisset>Ativo</option>
                            <option value="desligado" @isset($params['status']) @if($params['status'] == 'desligado') {{'selected'}} @endif @endisset>Desligado</option>
                        </select>
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="mt-3 col-auto">
                        <label for="projeto" class="form-label">Projeto:</label>
                        <select class="form-select" aria-label="projeto" id="projeto" name="projeto">
                            <option value="">Selecione...</option>
                            <option value="SCFV 6 à 14 anos" @isset($params['projeto']) @if($params['projeto'] == "SCFV 6 à 14 anos") {{'selected'}} @endif @endisset>SCFV 6 à 14 anos</option>
                            <option value="SCFV 15 à 17 anos" @isset($params['projeto']) @if($params['projeto'] == "SCFV 15 à 17 anos") {{'selected'}} @endif @endisset>SCFV 15 à 17 anos</option>
                            <option value="SENAR" @isset($params['projeto']) @if($params['projeto'] == "SENAR") {{'selected'}} @endif @endisset>SENAR</option>
                        </select>
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="mt-3 col-auto">
                        <label for="encaminhamento" class="form-label">Encaminhamento:</label>
                        <select class="form-select" aria-label="encaminhamento" id="encaminhamento" name="encaminhamento">
                            <option value="">Selecione...</option>
                            <option value="CRAS" @isset($params['encaminhamento']) @if($params['encaminhamento'] == "CRAS") {{'selected'}} @endif @endisset>CRAS</option>
                            <option value="Conselho Tutelar" @isset($params['encaminhamento']) @if($params['encaminhamento'] == "Conselho Tutelar") {{'selected'}} @endif @endisset>Conselho Tutelar</option>
                            <option value="Rede Socioassistencial" @isset($params['encaminhamento']) @if($params['encaminhamento'] == "Rede Socioassistencial") {{'selected'}} @endif @endisset>Rede Socioassistencial</option>
                        </select>
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="mt-3 col-auto">
                        <label for="bairro" class="form-label">Bairro:</label>
                        <select class="form-select" aria-label="bairro" name="bairro" id="bairro">
                            <option value="">Selecione...</option>
                           <option value="Água Azul" @isset($params['bairro']) @if($params['bairro'] == "Água Azul") {{'selected'}} @endif @endisset>Água Azul</option>
                            <option value="Água Amarela" @isset($params['bairro']) @if($params['bairro'] == "Água Amarela") {{'selected'}} @endif @endisset>Água Amarela</option>
                            <option value="Aquinos" @isset($params['bairro']) @if($params['bairro'] == "Aquinos") {{'selected'}} @endif @endisset>Aquinos</option>
                            <option value="Agrovila 2" @isset($params['bairro']) @if($params['bairro'] == "Agrovila 2") {{'selected'}} @endif @endisset>Agrovila 2</option>
                            <option value="Agrovila 3" @isset($params['bairro']) @if($params['bairro'] == "Agrovila 3") {{'selected'}} @endif @endisset>Agrovila 3</option>
                            <option value="Área Industrial" @isset($params['bairro']) @if($params['bairro'] == "Área Industrial") {{'selected'}} @endif @endisset>Área Industrial</option>
                            <option value="Cambará" @isset($params['bairro']) @if($params['bairro'] == "Cambará") {{'selected'}} @endif @endisset>Cambará</option>
                            <option value="Cambarazinho" @isset($params['bairro']) @if($params['bairro'] == "Cambarazinho") {{'selected'}} @endif @endisset>Cambarazinho</option>
                            <option value="Centro" @isset($params['bairro']) @if($params['bairro'] == "Centro") {{'selected'}} @endif @endisset>Centro</option>
                            <option value="Cerrado" @isset($params['bairro']) @if($params['bairro'] == "Cerrado") {{'selected'}} @endif @endisset>Cerrado</option>
                            <option value="Comum" @isset($params['bairro']) @if($params['bairro'] == "Comum") {{'selected'}} @endif @endisset>Comum</option>
                            <option value="Engº Maia" @isset($params['bairro']) @if($params['bairro'] == "Engº Maia") {{'selected'}} @endif @endisset>Engº Maia</option>
                            <option value="Jd Carolina" @isset($params['bairro']) @if($params['bairro'] == "Jd Carolina") {{'selected'}} @endif @endisset>Jd Carolina</option>
                            <option value="Jd Espanha 1" @isset($params['bairro']) @if($params['bairro'] == "Jd Espanha 1") {{'selected'}} @endif @endisset>Jd Espanha 1</option>
                            <option value="Jd Espanha 2" @isset($params['bairro']) @if($params['bairro'] == "Jd Espanha 2") {{'selected'}} @endif @endisset>Jd Espanha 2</option>
                            <option value="Jd Espanha 3" @isset($params['bairro']) @if($params['bairro'] == "Jd Espanha 3") {{'selected'}} @endif @endisset>Jd Espanha 3</option>
                            <option value="Jd Rossi" @isset($params['bairro']) @if($params['bairro'] == "Jd Rossi") {{'selected'}} @endif @endisset>Jd Rossi</option>
                            <option value="Jd Lúcia" @isset($params['bairro']) @if($params['bairro'] == "Jd Lúcia") {{'selected'}} @endif @endisset>Jd Lúcia</option>
                            <option value="Jd São Luiz" @isset($params['bairro']) @if($params['bairro'] == "Jd São Luiz") {{'selected'}} @endif @endisset>Jd São Luiz</option>
                            <option value="Jd São Pedro" @isset($params['bairro']) @if($params['bairro'] == "Jd São Pedro") {{'selected'}} @endif @endisset>Jd São Pedro</option>
                            <option value="Pirituba" @isset($params['bairro']) @if($params['bairro'] == "Pirituba") {{'selected'}} @endif @endisset>Pirituba</option>
                            <option value="Quarentei" @isset($params['bairro']) @if($params['bairro'] == "Quarentei") {{'selected'}} @endif @endisset>Quarentei</option>
                            <option value="Residencial Canadá" @isset($params['bairro']) @if($params['bairro'] == "Residencial Canadá") {{'selected'}} @endif @endisset>Residencial Canadá</option>
                            <option value="Residencial Mirto Couto" @isset($params['bairro']) @if($params['bairro'] == "Residencial Mirto Couto") {{'selected'}} @endif @endisset>Residencial Mirto Couto</option>
                            <option value="Santa Inês 1" @isset($params['bairro']) @if($params['bairro'] == "Santa Inês 1") {{'selected'}} @endif @endisset>Santa Inês 1</option>
                            <option value="Santa Inês 2" @isset($params['bairro']) @if($params['bairro'] == "Santa Inês 2") {{'selected'}} @endif @endisset>Santa Inês 2</option>
                            <option value="Santa Inês 3" @isset($params['bairro']) @if($params['bairro'] == "Santa Inês 3") {{'selected'}} @endif @endisset>Santa Inês 3</option>
                            <option value="Santa Inês 4" @isset($params['bairro']) @if($params['bairro'] == "Santa Inês 4") {{'selected'}} @endif @endisset>Santa Inês 4</option>
                            <option value="Serrinha" @isset($params['bairro']) @if($params['bairro'] == "Serrinha") {{'selected'}} @endif @endisset>Serrinha</option>
                            <option value="Taquarussu" @isset($params['bairro']) @if($params['bairro'] == "Taquarussu") {{'selected'}} @endif @endisset>Taquarussu</option>
                            <option value="Tomé" @isset($params['bairro']) @if($params['bairro'] == "Tomé") {{'selected'}} @endif @endisset>Tomé</option>
                            <option value="Turiba do Sul" @isset($params['bairro']) @if($params['bairro'] == "Turiba do Sul") {{'selected'}} @endif @endisset>Turiba do Sul</option>
                            <option value="Vila Bandeirantes" @isset($params['bairro']) @if($params['bairro'] == "Vila Bandeirantes") {{'selected'}} @endif @endisset>Vila Bandeirantes</option>
                            <option value="Vila Dom Silvio" @isset($params['bairro']) @if($params['bairro'] == "Vila Dom Silvio") {{'selected'}} @endif @endisset>Vila Dom Silvio</option>
                            <option value="Vila Esperança" @isset($params['bairro']) @if($params['bairro'] == "Vila Esperança") {{'selected'}} @endif @endisset>Vila Esperança</option>
                            <option value="Vila Cruzeiro" @isset($params['bairro']) @if($params['bairro'] == "Vila Cruzeiro") {{'selected'}} @endif @endisset>Vila Cruzeiro</option>
                        </select>
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="mt-3 col-auto">
                        <label for="escola" class="form-label">Escola:</label>
                        <select class="form-select" aria-label="escola" id="escola" name="escola">
                            <option value="" selected>Selecione...</option>
                            <option value="ALBERTO PEREIRA PROFESSOR" @isset($params['escola']) @if($params['escola'] == 'ALBERTO PEREIRA PROFESSOR') {{'selected'}} @endif @endisset>ALBERTO PEREIRA PROFESSOR</option>
                            <option value="DOROTY DAVID MUZEL PROFESSORA" @isset($params['escola']) @if($params['escola'] == 'DOROTY DAVID MUZEL PROFESSORA') {{'selected'}} @endif @endisset>DOROTY DAVID MUZEL PROFESSORA</option>
                            <option value="GABRIEL PINTO DE FARIA PROFESSOR" @isset($params['escola']) @if($params['escola'] == 'GABRIEL PINTO DE FARIA PROFESSOR') {{'selected'}} @endif @endisset>GABRIEL PINTO DE FARIA PROFESSOR</option>
                            <option value="JARDIM SANTA INES" @isset($params['escola']) @if($params['escola'] == 'JARDIM SANTA INES') {{'selected'}} @endif @endisset>JARDIM SANTA INES</option>
                            <option value="LEONCIO PIMENTEL" @isset($params['escola']) @if($params['escola'] == 'LEONCIO PIMENTEL') {{'selected'}} @endif @endisset>LEONCIO PIMENTEL</option>
                            <option value="MARIA TEREZA DE SOUSA FALCARELI PROFESSORA" @isset($params['escola']) @if($params['escola'] == 'MARIA TEREZA DE SOUSA FALCARELI PROFESSORA') {{'selected'}} @endif @endisset>MARIA TEREZA DE SOUSA FALCARELI PROFESSORA</option>
                        </select>
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="mt-3 col-2">
                        <label for="idade_de" class="form-label">Idade: </label>
                        <input min="1" max="100" type="number" class="form-control" id="idade_de" name="idade_de" value="{{$params['idade_de'] ?? ''}}" onchange="setIdadeAte(this.value);">
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="mt-6 col-auto">
                        <label for="idade_ate" class="form-label">e</label>
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="mt-5 col-2">
                        <input max="100" type="number" class="form-control" id="idade_ate" name="idade_ate" value="{{$params['idade_ate'] ?? ''}}">
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="mt-3 col-auto">
                        <label for="ordenar" class="form-label">Ordenar por:</label>
                        <select class="form-select" aria-label="ordenar" id="ordenar" name="ordenar">
                            <option value="data_inicio" @isset($params['ordenar']) @if($params['ordenar'] == 'data_inicio') {{'selected'}} @endif @endisset selected>Data início</option>
                            <option value="nome" @isset($params['ordenar']) @if($params['ordenar'] == 'nome') {{'selected'}} @endif @endisset>Nome</option>
                            <option value="data_nascimento" @isset($params['ordenar']) @if($params['ordenar'] == 'data_nascimento') {{'selected'}} @endif @endisset>Idade</option>
                            <option value="escola" @isset($params['ordenar']) @if($params['ordenar'] == 'escola') {{'selected'}} @endif @endisset>Escola</option>
                            <option value="encaminhamento" @isset($params['ordenar']) @if($params['ordenar'] == 'encaminhamento') {{'selected'}} @endif @endisset>Encaminhamento</option>
                        </select>
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="mt-3 col-auto">
                        <label for="em_ordem" class="form-label">Em ordem:</label>
                        <select class="form-select" aria-label="em_ordem" id="em_ordem" name="em_ordem">
                            <option value="asc" @isset($params['em_ordem']) @if($params['em_ordem'] == 'cres') {{'selected'}} @endif @endisset selected>Crescente</option>
                            <option value="desc" @isset($params['em_ordem']) @if($params['em_ordem'] == 'desc') {{'selected'}} @endif @endisset>Decrescente</option>
                        </select>
                    </div>

                    <div class="mt-4 col-12">
                        <button type="submit" class="btn btn-secondary">Gerar</button>
                        <button type="button" class="btn btn-outline-primary" onclick="limparCampos();">Limpar</button>
                    </div>
                </div>
            </form>
        </div>

        @isset($alunos)

            <div class="container p-0 mt-2">
                <div class="table-responsive">

                    @if(count($alunos) > 0)

                        <div class="container p-0 btn-left">
                            <b>{{$total_alunos}} alunos encontrado(s).</b>
                        </div>

                        <div class="container p-0 btn-right">
                            <form id="formResultadoRelatorio" class="g-3" action="{{route('relatorio.pdf')}}" method="POST">
                                @csrf
                                <input type="hidden" name="filtros" id="filtros">
                                <input type="hidden" name="alunos" id="alunos" value="{{ $alunos }}">
                                <button type="submit" formtarget="_blank" class="btn btn-info float-right btn-actions">
                                    <span class="material-symbols-outlined btn-actions" >picture_as_pdf</span>
                                    &nbsp
                                    Abrir PDF
                                </button>
                            </form>
                        </div>
                    @endif

                    <table class="table table-bordered table-striped mt-2" id="tabelaRelatorios">
                        <thead>
                            <tr>
                            <th width="22%" scope="col">Nome</th>
                            <th width="160px" scope="col">Idade</th>
                            <th width="34%" scope="col">Escola</th>
                            <th width="13%" scope="col">Encaminhamento</th>
                            <th width="110px" scope="col">Data início</th>
                            </tr>
                        </thead>
                        <tbody>
                        <input type="hidden" id="id" name="id">
                        @foreach($alunos as $aluno)

                            <tr class="{{ $aluno->data_desligamento ? "table-danger" : '' }}">
                                <input type="hidden" class="dados" value="{{ $aluno }}" >
                                <td><span class="d-inline-block text-truncate w-column-name">{{$aluno->nome}}</span></td>
                                <td><span class="d-inline-block text-truncate w-column-name">{{\Carbon\Carbon::parse($aluno->data_nascimento)->age}} anos</span></td>
                                <td><span class="d-inline-block text-truncate w-column-schol">{{$aluno->escola}}</span></td>
                                <td>{{$aluno->encaminhamento ?? ''}}</td>
                                <td>{{\Carbon\Carbon::parse($aluno->data_inicio)->format('d/m/Y')}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        @endisset
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script>
        $(document).ready(function () {

            var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-tt-toggle="tooltip"]'))
            var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                return new bootstrap.Tooltip(tooltipTriggerEl)
            });

            @isset($alunos)
                $("#filtros").val($("#formRelatorio").serialize());
            @endisset
        });

        function limparCampos() {
            $("#periodo_de").val("");
            $("#periodo_ate").val("");
            $("#status").val("");
            $("#encaminhamento").val("").change();
            $("#bairro").val("").change();
            $("#escola").val("").change();
        }

        function getFiltros() {
            return $("#formRelatorio").serialize();
        }

        function setPeriodoAte(data) {
            $("#periodo_ate").prop('min', data);
        }

        function setIdadeAte(idade) {
            $("#idade_ate").prop('min', idade);
        }
    </script>
</body>
</html>
