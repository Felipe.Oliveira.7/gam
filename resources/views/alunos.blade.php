<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="robots" content="noindex,nofollow,noarchive,nosnippet,noodp,notranslate,noimageindex" />
    <meta name="csrf-token" content="<?php echo csrf_token(); ?>">
    <title>Sistema de Cadastros de Alunos GAM</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
</head>
<style>
    .title{
        margin: 20px;
    }
    .row>*{
        padding-right: 0 !important;
        padding-left: 0 !important;
    }
    .row{
        margin: 5rem !important;
    }
    .top-bar{
        margin: 0 !important;
        padding-right: 1.5rem;
    }
    .search{
        margin: -4rem 0rem -2rem 0rem;
        height: 40px;
    }
    .icon-bar{
        font-size: 42px;
    }
    .badge{
        --bs-badge-font-size: 1rem !important;
    }
    .w-column-name{
        max-width: 320px;
    }
    .w-column-schol{
        max-width: 330px;
    }
</style>
<body>
    <div class="container-fluid p-0">

        <div class="row top-bar border-bottom bg-light bg-gradient">
            <nav class="navbar">
                <div class="col-4">
                    <div class="container-fluid">
                            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                    </div>
                </div>
                <div class="col-4 text-center mt-1">
                    <a href="/home">
                        <img src="{{URL::asset('imagens/logo.jpg')}}" alt="Logo" class="img-thumbnail" width="52">
                    </a>
                </div>
                <div class="col-4 pt-3 pb-3 d-flex flex-row-reverse">
                    <a href="/logout" class="btn btn-outline-danger">Sair</a>
                </div>
            </nav>
        </div>

        <div class="collapse" id="navbarToggleExternalContent">
            <div class="bg-secondary p-4">
                <h6 class="text-white"><a href="/home" class="link-light text-decoration-none">Home</a></h6>
                <h6 class="text-white"><a href="/usuarios" class="link-light text-decoration-none">Usuários</a></h6>
                <h6 class="text-white">Alunos</h6>
                <h6 class="text-white"><a href="/relatorio" class="link-light text-decoration-none">Relatórios</a></h6>
            </div>
        </div>

        <div class="container-fluid mt-2">
            <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/home">
                    Home
                    </a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    Gerenciamento de Alunos
                </li>
            </ol>
            </nav>
        </div>

        <div class="container-fluid">
            <h5 class="badge bg-light text-dark">
                GERENCIAMENTO DE ALUNOS
            </h5>
        </div>

        <br>

        <div class="m-5">
            <form id="formPesquisaAluno" action="{{route('alunos.index')}}" method="GET" >
                @csrf
                <div class="search">
                    <div class="form-group input-group rounded">
                        <span class="input-group-text border-0 material-symbols-outlined">
                        search
                        </span>
                        <input onchange="pesquisarAluno();" id="pesquisar" name="search" type="search" class="form-control rounded" placeholder=" Digite para pesquisar e tecle [enter]" aria-label="Search" aria-describedby="search-addon" value="{{$search ?? ''}}">
                        &nbsp;&nbsp;&nbsp;
                        <a href="{{route('aluno.criar')}}" class="text-white text-decoration-none"><button type="button" class="btn btn-primary rounded">Novo</button></a>
                    </div>
                </div>
            </form>
        </div>

        <div class="m-5">
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="tabelaAlunos">
                    <thead>
                        <tr>
                        <th width="180px" scope="col" class="text-center">Ações</th>
                        <th width="22%" scope="col">Nome</th>
                        <th width="31%" scope="col">Escola</th>
                        <th width="17%" scope="col">Programa</th>
                        <th width="110px" scope="col">Data início</th>
                        </tr>
                    </thead>
                    <tbody>
                    <input type="hidden" id="id" name="id">
                    @foreach($alunos as $aluno)
                        <tr class="{{ $aluno->data_desligamento ? "table-danger" : '' }}">
                            <input type="hidden" class="dados" value="{{ $aluno }}" >
                            <td scope="row">
                                <div class="text-center">
                                    @if($aluno->data_desligamento == null)
                                        <button id="btnDeletar" type="button" class="btn btn-sm btn-light p-0 bg-secondary bg-gradient" data-tt-toggle="tooltip" title="Deletar">
                                            <span class="material-symbols-outlined p-1 text-white fs-6">do_not_disturb_on</span>
                                        </button>
                                    @endif
                                    <button type="button" class="btn btn-sm btn-light p-0 bg-secondary bg-gradient" data-bs-toggle="modal" data-tt-toggle="tooltip" data-bs-placement="top" data-bs-target="#modalInfo" title="Editar">
                                        <a href="{{route('aluno.perfil',['id' => $aluno->id])}}" class="text-decoration-none"><span class="material-symbols-outlined p-1 text-white fs-6">edit</span></a>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-light p-0 bg-secondary bg-gradient" data-tt-toggle="tooltip" data-bs-placement="top" title="Ficha">
                                        <a href="{{route('aluno.ficha',['id' => $aluno->id])}}" class="text-decoration-none" target="_blank">
                                            <span class="material-symbols-outlined p-1 text-white fs-6">summarize</span>
                                        </a>
                                    </button>
                                    @if($aluno->data_desligamento == null)
                                        <button id="btnDesligar" type="button" class="btn btn-sm btn-light p-0 bg-secondary bg-gradient" data-tt-toggle="tooltip" data-bs-placement="top" title="Desligar">
                                            <span class="material-symbols-outlined p-1 text-white fs-6">no_accounts</span>
                                        </button>
                                    @endif
                                    <button type="button" class="btn btn-sm btn-light p-0 bg-secondary bg-gradient" data-tt-toggle="tooltip" data-bs-placement="top" title="Ocorrências do aluno">
                                        <a href="{{route('ocorrencias.aluno',['id' => $aluno->id])}}" class="text-decoration-none" target="_blank">
                                            <span class="material-symbols-outlined p-1 text-white fs-6">event_note</span>
                                        </a>
                                    </button>
                                </div>
                            </td>
                            <td><span class="d-inline-block text-truncate w-column-name">{{$aluno->nome}}</span></td>
                            <td><span class="d-inline-block text-truncate w-column-schol">{{$aluno->escola}}</span></td>
                            <td>{{$aluno->encaminhamento ?? ''}}</td>
                            <td>{{\Carbon\Carbon::parse($aluno->data_inicio)->format('d/m/Y')}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                 {!! $alunos->withQueryString()->links('pagination::bootstrap-5') !!}

                 <div class="modal fade" id="modalDel" tabindex="-1" aria-labelledby="titleDel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <span class="material-symbols-outlined">do_not_disturb_on</span>
                                &nbsp;&nbsp;&nbsp;
                                <h5 class="modal-title" id="titleDel">Deletar Aluno</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-success alert-dismissible fade show" id="mensagem-sucesso-delete" role="alert"></div>
                                <div class="alert alert-danger alert-dismissible fade show" id="mensagem-erro-delete" role="alert"></div>
                                <span>Deseja realmente deletar esse aluno?</span>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Não</button>
                                <button type="button" class="btn btn-danger" onclick="deletarAluno();">Sim</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="modalDesligar" tabindex="-1" aria-labelledby="titleDel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <span class="material-symbols-outlined">no_accounts</span>
                                &nbsp;&nbsp;&nbsp;
                                <h5 class="modal-title" id="titleDel">Desligar Aluno</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <form id="formDesligamento" class="g-3 needs-validation" novalidate>
                                @csrf
                                <div class="modal-body">
                                    <div class="alert alert-success alert-dismissible fade show" id="mensagem-sucesso-desligar" role="alert"></div>
                                    <div class="alert alert-danger alert-dismissible fade show" id="mensagem-erro-desligar" role="alert"></div>
                                    <span>*Data do desligamento:</span>
                                    <div class="mt-3 col-12">
                                        <input min="2000-01-01" max="{{ now()->toDateString('Y-m-d') }}" type="date" class="form-control" id="data_desligamento" name="data_desligamento" required>
                                    </div>
                                    <div class="mt-2 col-12">
                                        <label for="motivo_desligamento" class="form-label">*Motivo do desligamento:</label>
                                        <select class="form-select" aria-label="motivo_desligamento" id="motivo_desligamento" name="motivo_desligamento" required>
                                            <option value="" selected>Selecione...</option>
                                            <option value="Mudança de município" >Mudança de município</option>
                                            <option value="Mudança de período escolar" >Mudança de período escolar</option>
                                            <option value="Por motivos particulares" >Por motivos particulares</option>
                                            <option value="Idade superior ao permitido" >Idade superior ao permitido</option>
                                            <option value="Faltas consecutivas sem justificativa" >Faltas consecutivas sem justificativa</option>
                                            <option value="Falta de interesse nas atividades" >Falta de interesse nas atividades</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">cancelar</button>
                                    <button type="submit" class="btn btn-danger">Desligar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="{{URL::asset('js/mensagens.js')}}" type="text/javascript"></script>
    <script>

        $(document).ready(function () {

            var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-tt-toggle="tooltip"]'))
            var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                return new bootstrap.Tooltip(tooltipTriggerEl)
            });

            var forms = document.querySelectorAll('.needs-validation')

            Array.prototype.slice.call(forms)
                .forEach(function (form) {
                form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            });

            //AÇÕES DOS BOTÕES DELETAR E DESLIGAR ALUNOS
            $('table').on('click', 'button', function(event) {

                var data = JSON.parse($(this).closest('tr').find(".dados").val());

                if((event.currentTarget.id == 'btnDeletar')){

                    $("#id").val(data.id);
                    $("#modalDel").modal('show');
                }

                if((event.currentTarget.id == 'btnDesligar')){

                    $("#data_desligamento").val("");
                    $("#id").val(data.id);
                    $("#motivo_desligamento").val(data.motivo_desligamento).change();
                    $("#modalDesligar").modal('show');

                    if(data.data_desligamento != null) {
                        $("#data_desligamento").val(data.data_desligamento);
                    }
                }
            });

            // LÓGICA DE SUBMIT DO MODAL DE DELETE E DESLIGAR
            $("#mensagem-sucesso-desligar").hide();
            $("#mensagem-erro-desligar").hide();
            $("#mensagem-sucesso-delete").hide();
            $("#mensagem-erro-delete").hide();

            $("#formDesligamento").submit(function(event){

                if($("#formDesligamento").valid()) {

                    event.preventDefault();

                    desligarAluno(this);
                }
                return false;
            });

        });

        function desligarAluno(form) {

            var url = '{{ route("aluno.desligar", ":id") }}';
            url = url.replace(":id", $("#id").val());

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: url,
                data: $(form).serialize(),
                dataType: "json",
                type: "PUT",
                success: function (data) {

                    $("#mensagem-sucesso-desligar").hide();
                    $("#mensagem-erro-desligar").hide();

                    if(data.success){
                        $("#mensagem-sucesso-desligar span").remove();
                        $("#mensagem-sucesso-desligar").append("<span>" + data.message + "</span>");
                        $("#mensagem-sucesso-desligar").show().delay(3200).fadeOut();

                        window.setTimeout(function(){
                            window.location.reload();
                        }, 3200);
                    }else{
                        $("#mensagem-erro-desligar").append("<span>" + data.message + "</span>");
                        $("#mensagem-erro-desligar").show();
                    }
                }
            });
        }

        function deletarAluno() {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{route('aluno.deletar')}}",
                data: {
                    id: $("#id").val(),
                },
                dataType: "json",
                type: "DELETE",
                success: function (data) {

                    $("#mensagem-sucesso-delete").hide();
                    $("#mensagem-erro-delete").hide();

                    if(data.success){
                        $("#mensagem-sucesso-delete span").remove();
                        $("#mensagem-sucesso-delete").append("<span>" + data.message + "</span>");
                        $("#mensagem-sucesso-delete").show().delay(3200).fadeOut();

                        window.setTimeout(function(){
                            window.location.reload();
                        }, 3200);
                    }else{
                        $("#mensagem-erro-delete").append("<span>" + data.message + "</span>");
                        $("#mensagem-erro-delete").show();
                    }
                }
            });
        }

        function pesquisarAluno() {

            $("#formPesquisaAluno").submit(function(event){

                var response = false;

                if($('#pesquisar').val() != ''){
                    response = true;
                }

                return response;
            });
        }

        function populaTabela(dados) {

            $('#tabelaAlunos tbody tr').remove();
            var html = "";
            for(var i = 0; i < dados.length; i++){

                html += '<tr>'+
                            "<input type='hidden' class='dados' value='" + JSON.stringify(dados[i]) + "' >"+
                            "<td scope='row'>" +
                                "<div class='text-center'>"+
                                    "<button type='button' class='btn btn-sm btn-light p-0 bg-secondary bg-gradient' data-bs-toggle='modal' data-tt-toggle='tooltip' data-bs-target='#modalDel' title='Deletar'>"+
                                        "<span class='material-symbols-outlined p-1 text-white fs-6'>do_not_disturb_on</span>"+
                                    "</button>"+
                                    "<button type='button' class='btn btn-sm btn-light p-0 bg-secondary bg-gradient' data-bs-toggle='modal' data-tt-toggle='tooltip' data-bs-placement='top' data-bs-target='#modalInfo' title='Editar'>"+
                                        "<span class='material-symbols-outlined p-1 text-white fs-6'>edit</span>"+
                                    "</button>"+
                                    "<button type='button' class='btn btn-sm btn-light p-0 bg-secondary bg-gradient' data-tt-toggle='tooltip' data-bs-placement='top' title='Ficha'>"+
                                        "<span class='material-symbols-outlined p-1 text-white fs-6'>summarize</span>"+
                                    "</button>"+
                                "</div>"+
                            "</td>" +
                            '<td>' + dados[i].nome + '</td>' +
                            '<td>' + dados[i].escola + '</td>' +
                            '<td>' + dados[i].encaminhamento + '</td>' +
                            '<td>' + dados[i].data_inicio + '</td>' +
                        '</tr>';
            }

            $('#tabelaAlunos tbody').append(html);
        }
    </script>
</body>
</html>
