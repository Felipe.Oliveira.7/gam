<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="robots" content="noindex,nofollow,noarchive,nosnippet,noodp,notranslate,noimageindex" />
    <meta name="csrf-token" content="<?php echo csrf_token(); ?>">
    <title>Sistema de Cadastros de Alunos GAM</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
</head>
<style>
    .title{
        margin: 20px;
    }
    .row>*{
        padding-right: 0 !important;
        padding-left: 0 !important;
    }
    .row{
        margin: 5rem !important;
    }
    .top-bar{
        margin: 0 !important;
        padding-right: 1.5rem;
    }
    .search{
        margin: 3rem 5rem -4rem 5rem;
    }
    .icon-bar{
        font-size: 42px;
    }
    .badge{
        --bs-badge-font-size: 1rem !important;
    }
    .btn-actions{
        display: flex;
        align-items: center;
        justify-content: center;
    }
</style>
<body>
    <div class="container-fluid p-0">

        <div class="row top-bar border-bottom bg-light bg-gradient">
            <nav class="navbar">
                <div class="col-4">
                    <div class="container-fluid">
                            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                    </div>
                </div>
                <div class="col-4 text-center mt-1">
                    <a href="/home">
                        <img src="{{URL::asset('imagens/logo.jpg')}}" alt="Logo" class="img-thumbnail" width="52">
                    </a>
                </div>
                <div class="col-4 pt-3 pb-3 d-flex flex-row-reverse">
                    <a href="/logout" class="btn btn-outline-danger">Sair</a>
                </div>
            </nav>
        </div>

        <div class="collapse" id="navbarToggleExternalContent">
            <div class="bg-secondary p-4">
                <h6 class="text-white"><a href="/home" class="link-light text-decoration-none">Home</a></h6>
                <h6 class="text-white"><a href="/usuarios" class="link-light text-decoration-none">Usuários</a></h6>
                <h6 class="text-white"><a href="/alunos" class="link-light text-decoration-none">Alunos</a></h6>
                <h6 class="text-white"><a href="/relatorio" class="link-light text-decoration-none">Relatórios</a></h6>
            </div>
        </div>

        <div class="container-fluid mt-2">
            <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/home">
                    Home
                    </a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                    <a href="/alunos">
                        Gerenciamento de Alunos
                    </a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    {{isset($aluno->id) ? 'Edição de Aluno' : 'Cadastro de Aluno' }}
                </li>
            </ol>
            </nav>
        </div>

        <div class="container-fluid mb-3">
            <h5 class="badge bg-light text-dark">
                {{isset($aluno->id) ? 'EDIÇÃO DE ALUNO' : 'CADASTRO DE ALUNO' }}
            </h5>
        </div>

        <div class="container p-0 mb-2 d-flex flex-row-reverse bd-highlight">

            @isset($aluno)
                @if($aluno->data_desligamento != null)
                    <span class="badge rounded-pill bg-danger btn-actions">
                        <span class="material-symbols-outlined btn-actions" >do_not_disturb_on</span>
                        &nbsp;
                        Aluno Desligado</span>
                        &nbsp;&nbsp;&nbsp;
                @endif
            @endisset

            @isset($aluno->id)
                <button id="btnBloquear" type="button" class="btn btn-warning btn-actions" onclick="habilitarFormulario();">
                    <span class="material-symbols-outlined btn-actions" >toggle_off</span>
                    &nbsp;
                    Desbloquear Edição
                </button>
                &nbsp;&nbsp;&nbsp;
                <button id="btnFicha" type="button" class="btn btn-primary btn-actions">
                    <a href="{{route('aluno.ficha',['id' => $aluno->id])}}" class="text-decoration-none text-white btn-actions" target="_blank">
                        <span class="material-symbols-outlined btn-actions" >summarize</span>
                        &nbsp;
                        Abrir Ficha do Aluno
                    </a>
                </button>
            @endisset
        </div>

        <div class="container border p-4 mb-4">

            <div class="container p-0">
                <div class="alert alert-success alert-dismissible fade show" id="mensagem-sucesso" role="alert"></div>
                <div class="alert alert-danger alert-dismissible fade show" id="mensagem-erro" role="alert"></div>
            </div>

            <form id="formAluno" class="g-3 needs-validation" novalidate>
                @isset($aluno->id)<fieldset id="fieldsetFormAluno" disabled>@endisset
                    @csrf
                    <h4 class="mb-4">
                        <span class="badge rounded-pill bg-secondary">
                            DADOS DO ALUNO
                        </span>
                    </h4>

                    <img id="foto-aluno" src="@if(isset($aluno->foto_nome)) {{URL::asset('uploads/' . $aluno->foto_nome . '.' .$aluno->foto_tipo)}} @else {{URL::asset('uploads/empty.png')}} @endif" width="300" class="img-thumbnail float-end" alt="">

                    <input type="hidden" name="id" id="id" value="{{$aluno->id ?? ''}}">

                    <div class="mb-2 col-7">
                        <label for="foto" class="form-label">@if(!isset($aluno->id)) {{'*'}} @endif Foto:</label>
                        <input type="file" class="form-control" id="foto" name="foto" @if(!isset($aluno->id)) {{ 'required' }} @endif>
                    </div>
                    <div class="mb-2 col-7">
                        <label for="nome" class="form-label">*Nome:</label>
                        <input type="text" class="form-control" id="nome" name="nome" maxlength="150" value="{{$aluno->nome ?? ''}}" required>
                    </div>
                    <div class="mb-2 col-2">
                        <label for="serie" class="form-label">*Série:</label>
                        <input type="number" min="0" class="form-control" id="serie" name="serie" value="{{$aluno->serie ?? ''}}" required>
                    </div>
                    <div class="mb-2 col-5">
                        <label for="escola" class="form-label">*Escola:</label>
                        <select class="form-select" aria-label="escola" id="escola" name="escola" required>
                            <option value="" selected>Selecione...</option>
                            <option value="ALBERTO PEREIRA PROFESSOR" @isset($aluno->escola) @if($aluno->escola == "ALBERTO PEREIRA PROFESSOR") {{'selected'}} @endif @endisset>ALBERTO PEREIRA PROFESSOR</option>
                            <option value="DOROTY DAVID MUZEL PROFESSORA" @isset($aluno->escola) @if($aluno->escola == "DOROTY DAVID MUZEL PROFESSORA") {{'selected'}} @endif @endisset>DOROTY DAVID MUZEL PROFESSORA</option>
                            <option value="GABRIEL PINTO DE FARIA PROFESSOR" @isset($aluno->escola) @if($aluno->escola == "GABRIEL PINTO DE FARIA PROFESSOR") {{'selected'}} @endif @endisset>GABRIEL PINTO DE FARIA PROFESSOR</option>
                            <option value="JARDIM SANTA INES" @isset($aluno->escola) @if($aluno->escola == "JARDIM SANTA INES") {{'selected'}} @endif @endisset>JARDIM SANTA INES</option>
                            <option value="LEONCIO PIMENTEL" @isset($aluno->escola) @if($aluno->escola == "LEONCIO PIMENTEL") {{'selected'}} @endif @endisset>LEONCIO PIMENTEL</option>
                            <option value="MARIA TEREZA DE SOUSA FALCARELI PROFESSORA" @isset($aluno->escola) @if($aluno->escola == "MARIA TEREZA DE SOUSA FALCARELI PROFESSORA") {{'selected'}} @endif @endisset>MARIA TEREZA DE SOUSA FALCARELI PROFESSORA</option>
                        </select>
                    </div>
                    <div class="mb-2 col-2">
                        <label for="Data de nascimento" class="form-label">*Data de Nascimento:</label>
                        <input min="1920-01-01" max="{{ now()->toDateString('Y-m-d') }}" type="date" class="form-control" id="data_nascimento" name="data_nascimento" value="{{$aluno->data_nascimento ?? ''}}" required>
                    </div>
                    <div class="mb-2 col-7">
                        <label for="Endereco" class="form-label">*Endereço:</label>
                        <input type="text" class="form-control" id="endereco" name="endereco" maxlength="200" value="{{$aluno->endereco ?? ''}}" required>
                    </div>
                    <div class="mb-2 col-2">
                        <label for="numero" class="form-label">Nº:</label>
                        <input type="number" min="0" class="form-control" id="numero" name="numero" value="{{$aluno->numero ?? ''}}">
                    </div>
                    <div class="mb-2 col-3">
                        <label for="Bairro" class="form-label">*Bairro:</label>
                        <select class="form-select" aria-label="bairro" name="bairro" id="bairro" required>
                            <option value="">Selecione...</option>
                            <option value="Água Azul" @isset($aluno->bairro) @if($aluno->bairro == "Água Azul") {{'selected'}} @endif @endisset>Água Azul</option>
                            <option value="Água Amarela" @isset($aluno->bairro) @if($aluno->bairro == "Água Amarela") {{'selected'}} @endif @endisset>Água Amarela</option>
                            <option value="Aquinos" @isset($aluno->bairro) @if($aluno->bairro == "Aquinos") {{'selected'}} @endif @endisset>Aquinos</option>
                            <option value="Agrovila 2" @isset($aluno->bairro) @if($aluno->bairro == "Agrovila 2") {{'selected'}} @endif @endisset>Agrovila 2</option>
                            <option value="Agrovila 3" @isset($aluno->bairro) @if($aluno->bairro == "Agrovila 3") {{'selected'}} @endif @endisset>Agrovila 3</option>
                            <option value="Área Industrial" @isset($aluno->bairro) @if($aluno->bairro == "Área Industrial") {{'selected'}} @endif @endisset>Área Industrial</option>
                            <option value="Cambará" @isset($aluno->bairro) @if($aluno->bairro == "Cambará") {{'selected'}} @endif @endisset>Cambará</option>
                            <option value="Cambarazinho" @isset($aluno->bairro) @if($aluno->bairro == "Cambarazinho") {{'selected'}} @endif @endisset>Cambarazinho</option>
                            <option value="Centro" @isset($aluno->bairro) @if($aluno->bairro == "Centro") {{'selected'}} @endif @endisset>Centro</option>
                            <option value="Cerrado" @isset($aluno->bairro) @if($aluno->bairro == "Cerrado") {{'selected'}} @endif @endisset>Cerrado</option>
                            <option value="Comum" @isset($aluno->bairro) @if($aluno->bairro == "Comum") {{'selected'}} @endif @endisset>Comum</option>
                            <option value="Engº Maia" @isset($aluno->bairro) @if($aluno->bairro == "Engº Maia") {{'selected'}} @endif @endisset>Engº Maia</option>
                            <option value="Jd Carolina" @isset($aluno->bairro) @if($aluno->bairro == "Jd Carolina") {{'selected'}} @endif @endisset>Jd Carolina</option>
                            <option value="Jd Espanha 1" @isset($aluno->bairro) @if($aluno->bairro == "Jd Espanha 1") {{'selected'}} @endif @endisset>Jd Espanha 1</option>
                            <option value="Jd Espanha 2" @isset($aluno->bairro) @if($aluno->bairro == "Jd Espanha 2") {{'selected'}} @endif @endisset>Jd Espanha 2</option>
                            <option value="Jd Espanha 3" @isset($aluno->bairro) @if($aluno->bairro == "Jd Espanha 3") {{'selected'}} @endif @endisset>Jd Espanha 3</option>
                            <option value="Jd Rossi" @isset($aluno->bairro) @if($aluno->bairro == "Jd Rossi") {{'selected'}} @endif @endisset>Jd Rossi</option>
                            <option value="Jd Lúcia" @isset($aluno->bairro) @if($aluno->bairro == "Jd Lúcia") {{'selected'}} @endif @endisset>Jd Lúcia</option>
                            <option value="Jd São Luiz" @isset($aluno->bairro) @if($aluno->bairro == "Jd São Luiz") {{'selected'}} @endif @endisset>Jd São Luiz</option>
                            <option value="Jd São Pedro" @isset($aluno->bairro) @if($aluno->bairro == "Jd São Pedro") {{'selected'}} @endif @endisset>Jd São Pedro</option>
                            <option value="Pirituba" @isset($aluno->bairro) @if($aluno->bairro == "Pirituba") {{'selected'}} @endif @endisset>Pirituba</option>
                            <option value="Quarentei" @isset($aluno->bairro) @if($aluno->bairro == "Quarentei") {{'selected'}} @endif @endisset>Quarentei</option>
                            <option value="Residencial Canadá" @isset($aluno->bairro) @if($aluno->bairro == "Residencial Canadá") {{'selected'}} @endif @endisset>Residencial Canadá</option>
                            <option value="Residencial Mirto Couto" @isset($aluno->bairro) @if($aluno->bairro == "Residencial Mirto Couto") {{'selected'}} @endif @endisset>Residencial Mirto Couto</option>
                            <option value="Santa Inês 1" @isset($aluno->bairro) @if($aluno->bairro == "Santa Inês 1") {{'selected'}} @endif @endisset>Santa Inês 1</option>
                            <option value="Santa Inês 2" @isset($aluno->bairro) @if($aluno->bairro == "Santa Inês 2") {{'selected'}} @endif @endisset>Santa Inês 2</option>
                            <option value="Santa Inês 3" @isset($aluno->bairro) @if($aluno->bairro == "Santa Inês 3") {{'selected'}} @endif @endisset>Santa Inês 3</option>
                            <option value="Santa Inês 4" @isset($aluno->bairro) @if($aluno->bairro == "Santa Inês 4") {{'selected'}} @endif @endisset>Santa Inês 4</option>
                            <option value="Serrinha" @isset($aluno->bairro) @if($aluno->bairro == "Serrinha") {{'selected'}} @endif @endisset>Serrinha</option>
                            <option value="Taquarussu" @isset($aluno->bairro) @if($aluno->bairro == "Taquarussu") {{'selected'}} @endif @endisset>Taquarussu</option>
                            <option value="Tomé" @isset($aluno->bairro) @if($aluno->bairro == "Tomé") {{'selected'}} @endif @endisset>Tomé</option>
                            <option value="Turiba do Sul" @isset($aluno->bairro) @if($aluno->bairro == "Turiba do Sul") {{'selected'}} @endif @endisset>Turiba do Sul</option>
                            <option value="Vila Bandeirantes" @isset($aluno->bairro) @if($aluno->bairro == "Vila Bandeirantes") {{'selected'}} @endif @endisset>Vila Bandeirantes</option>
                            <option value="Vila Dom Silvio" @isset($aluno->bairro) @if($aluno->bairro == "Vila Dom Silvio") {{'selected'}} @endif @endisset>Vila Dom Silvio</option>
                            <option value="Vila Esperança" @isset($aluno->bairro) @if($aluno->bairro == "Vila Esperança") {{'selected'}} @endif @endisset>Vila Esperança</option>
                            <option value="Vila Cruzeiro" @isset($aluno->bairro) @if($aluno->bairro == "Vila Cruzeiro") {{'selected'}} @endif @endisset>Vila Cruzeiro</option>
                        </select>
                    </div>
                    <div class="mb-2 col-4">
                        <label for="telefone" class="form-label">Telefone:</label>
                        <input type="text" class="form-control" id="telefone" name="telefone" data-mask="(00)00009-0000" data-mask-selectonfocus="true" value="{{$aluno->telefone ?? ''}}">
                    </div>
                    <div class="mb-2 col-2">
                        <label for="quantidade_familiares" class="form-label">*Quantidade Familiares:</label>
                        <input type="number" min="0" class="form-control" id="qtd_familiares" name="qtd_familiares" value="{{$aluno->qtd_familiares ?? ''}}" required>
                    </div>
                    <div class="mb-2 col-3">
                        <label for="nis" class="form-label">NIS:</label>
                        <input type="text" class="form-control" id="nis" name="nis" value="{{$aluno->nis ?? ''}}">
                    </div>
                    <div class="mb-2 col-2">
                        <label for="renda_familiar" class="form-label">*Renda Familiar:</label>
                        <input type="text" class="form-control" id="renda_familiar" name="renda_familiar" value="{{$aluno->renda_familiar ?? ''}}" required>
                    </div>

                    <h4 class="mt-5 mb-5">
                        <span class="badge rounded-pill bg-secondary">
                            DADOS DOS RESPONSÁVEIS
                        </span>
                    </h4>

                    <div class="mb-2 col-7">
                        <label for="pai" class="form-label">*Pai:</label>
                        <input type="text" class="form-control" id="nome_pai" name="nome_pai" maxlength="150" value="{{$aluno->nome_pai ?? ''}}" required>
                    </div>
                    <div class="mb-2 col-7">
                        <label for="local_trabalho_pai" class="form-label">Local de Trabalho do Pai:</label>
                        <input type="text" class="form-control" id="local_trabalho_pai" name="local_trabalho_pai" maxlength="200" value="{{$aluno->local_trabalho_pai ?? ''}}">
                    </div>
                    <div class="mb-2 col-4">
                        <label for="telefonePai" class="form-label">Telefone Pai:</label>
                        <input type="tel" class="form-control" id="telefone_pai" name="telefone_pai" value="{{$aluno->telefone_pai ?? ''}}">
                    </div>
                    <div class="mb-2 col-7">
                        <label for="mae" class="form-label">*Mãe:</label>
                        <input type="text" class="form-control" id="nome_mae" name="nome_mae" maxlength="150" value="{{$aluno->nome_mae ?? ''}}" required>
                    </div>
                    <div class="mb-2 col-7">
                        <label for="local_trabalho_mae" class="form-label">Local de Trabalho da Mãe:</label>
                        <input type="text" class="form-control" id="local_trabalho_mae" name="local_trabalho_mae" maxlength="200" value="{{$aluno->local_trabalho_mae ?? ''}}">
                    </div>
                    <div class="mb-2 col-4">
                        <label for="telefoneMae" class="form-label">Telefone Mãe:</label>
                        <input type="tel" class="form-control" id="telefone_mae" name="telefone_mae" value="{{$aluno->telefone_mae ?? ''}}">
                    </div>
                    <div class="mb-2 col-2">
                        <label for="DataDeInicio" class="form-label">*Data de Início:</label>
                        <input min="1920-01-01" max="{{ now()->toDateString('Y-m-d') }}" type="date" class="form-control" id="data_inicio" name="data_inicio" value="{{$aluno->data_inicio ?? ''}}" required>
                    </div>
                    <div class="mb-2 col-3">
                        <label for="projeto" class="form-label">*Projeto:</label>
                        <select class="form-select" aria-label="projeto" id="projeto" name="projeto" required>
                            <option value="">Selecione...</option>
                            <option value="SCFV 6 à 14 anos" @isset($aluno->projeto) @if($aluno->projeto == "SCFV 6 à 14 anos") {{'selected'}} @endif @endisset>SCFV 6 à 14 anos</option>
                            <option value="SCFV 15 à 17 anos" @isset($aluno->projeto) @if($aluno->projeto == "SCFV 15 à 17 anos") {{'selected'}} @endif @endisset>SCFV 15 à 17 anos</option>
                            <option value="SENAR" @isset($aluno->projeto) @if($aluno->projeto == "SENAR") {{'selected'}} @endif @endisset>SENAR</option>
                        </select>
                    </div>
                    <div class="mb-2 col-3">
                        <label for="encaminhamento" class="form-label">*Encaminhamento:</label>
                        <select class="form-select" aria-label="encaminhamento" id="encaminhamento" name="encaminhamento" required>
                            <option value="">Selecione...</option>
                            <option value="CRAS" @isset($aluno->encaminhamento) @if($aluno->encaminhamento == "CRAS") {{'selected'}} @endif @endisset>CRAS</option>
                            <option value="Conselho Tutelar" @isset($aluno->encaminhamento) @if($aluno->encaminhamento == "Conselho Tutelar") {{'selected'}} @endif @endisset>Conselho Tutelar</option>
                            <option value="Rede Socioassistencial" @isset($aluno->encaminhamento) @if($aluno->encaminhamento == "Rede Socioassistencial") {{'selected'}} @endif @endisset>Rede Socioassistencial</option>
                        </select>
                    </div>
                    <div class="mb-2 col-2">
                    <label for="data_desligamento" class="form-label">Data de desligamento:</label>
                        <input min="2000-01-01" max="{{ now()->toDateString('Y-m-d') }}" type="date" class="form-control" id="data_desligamento" name="data_desligamento" value="{{$aluno->data_desligamento ?? ''}}">
                    </div>
                    <div class="mt-2 mb-2 col-3">
                        <label for="motivo_desligamento" class="form-label">Motivo do desligamento:</label>
                        <select class="form-select" aria-label="motivo_desligamento" id="motivo_desligamento" name="motivo_desligamento">
                            <option value="" selected>Selecione...</option>
                            <option value="Mudança de município" @isset($aluno->motivo_desligamento) @if($aluno->motivo_desligamento == "Mudança de município") {{'selected'}} @endif @endisset>Mudança de município</option>
                            <option value="Mudança de período escolar" @isset($aluno->motivo_desligamento) @if($aluno->motivo_desligamento == "Mudança de período escolar") {{'selected'}} @endif @endisset>Mudança de período escolar</option>
                            <option value="Por motivos particulares" @isset($aluno->motivo_desligamento) @if($aluno->motivo_desligamento == "Por motivos particulares") {{'selected'}} @endif @endisset>Por motivos particulares</option>
                            <option value="Idade superior ao permitido" @isset($aluno->motivo_desligamento) @if($aluno->motivo_desligamento == "Idade superior ao permitido") {{'selected'}} @endif @endisset>Idade superior ao permitido</option>
                            <option value="Faltas consecutivas sem justificativa" @isset($aluno->motivo_desligamento) @if($aluno->motivo_desligamento == "Faltas consecutivas sem justificativa") {{'selected'}} @endif @endisset>Faltas consecutivas sem justificativa</option>
                            <option value="Falta de interesse nas atividades" @isset($aluno->motivo_desligamento) @if($aluno->motivo_desligamento == "Falta de interesse nas atividades") {{'selected'}} @endif @endisset>Falta de interesse nas atividades</option>
                        </select>
                    </div>
                    <div class="mb-4 col-7">
                        <label for="obs" class="form-label">Observação:</label>
                        <textarea class="form-control" id="observacao" name="observacao" rows="10">{{$aluno->observacao ?? ''}}</textarea>
                    </div>

                    <div class="col-12">
                        <button type="submit" class="btn btn-success mb-3">Salvar</button>
                    </div>
                @isset($aluno->id)</fieldset>@endisset
            </form>
        </div>

        <div class="modal fade" id="modalFicha" tabindex="-1" aria-labelledby="titleFicha" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <span class="material-symbols-outlined">summarize</span>
                        &nbsp;&nbsp;&nbsp;
                        <h5 class="modal-title" id="titleFicha">Imprimir Ficha</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <span>Deseja imprimir a ficha desse aluno?</span>
                        <input type="hidden" id="idAluno">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Não</button>
                        <button type="button" class="btn btn-primary" onclick="VisualizarFichaAluno();">Sim</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js" type="text/javascript"></script>
    <script src="{{URL::asset('js/mensagens.js')}}" type="text/javascript"></script>
    <script>

    $(document).ready(function(){

        var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-tt-toggle="tooltip"]'))
        var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
            return new bootstrap.Tooltip(tooltipTriggerEl)
        })

        var forms = document.querySelectorAll('.needs-validation')

        Array.prototype.slice.call(forms)
            .forEach(function (form) {

            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
            });

            $("#btnSalvar").addClass('disabled');

            $('#mensagem-sucesso').hide();
            $('#mensagem-erro').hide();

            $('#telefone').mask('(00)00009-0000');
            $('#telefone_pai').mask('(00)00009-0000');
            $('#telefone_mae').mask('(00)00009-0000');
            $("#nis").mask('000.000000.00-00');
            $('#renda_familiar').maskMoney({
              prefix:'R$ ',
              allowNegative: true,
              thousands:'.', decimal:',',
              affixesStay: true,
              allowNegative: false
            });

            $("#formAluno").submit(function(event){

                if($("#formAluno").valid()) {

                    event.preventDefault();

                    var id = $("#id").val();

                    if(id.length == 0) {
                        cadastrarAluno(this);
                    } else {
                        editarAluno(this);
                    }
                }

                return false;
            });
        });

        function cadastrarAluno(form) {

            var formData = new FormData(form);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{route('aluno.novo')}}",
                data: formData,
                cache: false,
                dataType: 'json',
                contentType: false,
                processData: false,
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                        myXhr.upload.addEventListener('progress', function() {
                        }, false);
                    }
                    return myXhr;
                },
                type: "POST",
                success: function (data) {

                    $("#mensagem-sucesso").hide();
                    $("#mensagem-erro").hide();

                    if(data.success){

                        $("#idAluno").val(data.id);

                        limparCampos();
                        rolarInicioForm();

                        var form = document.getElementById("formAluno");
                        form.classList.remove('was-validated');

                        window.setTimeout(function(){
                            $("#mensagem-sucesso span").remove();
                            $("#mensagem-sucesso").append("<span>" + data.message + "</span>");
                            $("#mensagem-sucesso").show().delay(3200).fadeOut();
                            $("#modalFicha").modal('show');
                        }, 3000);

                    }else{
                        rolarInicioForm();

                        window.setTimeout(function(){
                            $("#mensagem-erro span").remove();
                            $("#mensagem-erro").append("<span>" + data.message + "</span>");
                            $("#mensagem-erro").show();
                        }, 3000);
                    }
                }
            });
        }

        function editarAluno(form) {

            var formData = new FormData(form);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                url: "{{route('aluno.editar')}}",
                data: formData,
                cache: false,
                dataType: "json",
                contentType: false,
                processData: false,
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                        myXhr.upload.addEventListener('progress', function() {
                        }, false);
                    }
                    return myXhr;
                },
                type: "POST",
                success: function (data) {

                    $("#mensagem-sucesso").hide();
                    $("#mensagem-erro").hide();

                    if(data.success){

                        $("#idAluno").val(data.id);

                        limparCampos();
                        rolarInicioForm();

                        var form = document.getElementById("formAluno");
                        form.classList.remove('was-validated');
                        window.setTimeout(function(){
                            $("#mensagem-sucesso span").remove();
                            $("#mensagem-sucesso").append("<span>" + data.message + "</span>");
                            $("#mensagem-sucesso").show().delay(3200).fadeOut();
                            $("#modalFicha").modal('show');
                        }, 3000);

                    }else{
                        rolarInicioForm();

                        window.setTimeout(function(){
                            $("#mensagem-erro span").remove();
                            $("#mensagem-erro").append("<span>" + data.message + "</span>");
                            $("#mensagem-erro").show();
                        }, 3000);
                    }
                }
            });
        }

        function rolarInicioForm(){

            $('html, body').animate({
                scrollTop: $("#foto").offset().top
            }, 1000, function() {
                $("#foto").focus();
            });
        }

        function limparCampos() {

            $('#foto-aluno').attr('src', "{{URL::asset('uploads/empty.png')}}");
            $("#id").val("");
            $("#foto").val("");
            $("#nome").val("");
            $("#serie").val("");
            $("#escola").val("").change();
            $("#data_nascimento").val("");
            $("#endereco").val("");
            $("#numero").val("");
            $("#bairro").val("").change();
            $("#telefone").val("");
            $("#qtd_familiares").val("");
            $("#nis").val("");
            $("#renda_familiar").val("");
            $("#nome_pai").val("");
            $("#local_trabalho_pai").val("");
            $("#local_trabalho_mae").val("");
            $("#telefone_pai").val("");
            $("#nome_mae").val("");
            $("#telefone_mae").val("");
            $("#data_inicio").val("");
            $("#projeto").val("").change();
            $("#encaminhamento").val("").change();
            $("#data_desligamento").val("");
            $("#motivo_desligamento").val("");
            $("#observacao").val("").change();
        }

        function VisualizarFichaAluno() {
            var url = '{{ route("aluno.ficha", ":id") }}';
            url = url.replace(':id', $("#idAluno").val());
            window.open(url, '_blank');
            $("#modalFicha").modal('hide');
        }

        function habilitarFormulario() {

            var fieldset = $("#fieldsetFormAluno").prop('disabled');
            $("#fieldsetFormAluno").prop('disabled', !fieldset);

            if(fieldset) {
                $("#btnBloquear").removeClass('btn-warning');
                $("#btnBloquear").addClass('btn-danger');
                $("#btnBloquear").html('<span class="material-symbols-outlined" style="display: flex;align-items: center;justify-content: center;">toggle_on</span>&nbsp;&nbsp;Bloquear Edição');
            }else{
                $("#btnBloquear").removeClass('btn-danger');
                $("#btnBloquear").addClass('btn-warning');
                $("#btnBloquear").html('<span class="material-symbols-outlined" style="display: flex;align-items: center;justify-content: center;">toggle_off</span>&nbsp;&nbsp;Desbloquear Edição');
            }
        }

    </script>
</body>
</html>
