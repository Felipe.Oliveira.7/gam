<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="robots" content="noindex,nofollow,noarchive,nosnippet,noodp,notranslate,noimageindex" />
    <meta name="csrf-token" content="<?php echo csrf_token(); ?>">
    <title>Sistema de Cadastros de Alunos GAM</title>
</head>
<style>
    body{
        background-color: #fff;
        padding: 2rem;
    }
    h2{
        font-size: 16px;
    }
    h3{
        font-size: 14px;
    }
    span{
        font-size: 12px;
    }
    p{
        font-size: 13px;
        margin-bottom: -0.5rem;
    }
    .float-right{
        float: right;
    }
    .float-left{
        float: left;
    }
    .text-center{
        text-align: center;
    }
    .clear-both{
        clear: both;
    }
    .mr-0{
        margin-right: 0rem;
    }
    .mt-0{
        margin-top: 0rem;
    }
    .mt-12{
        margin-top: 12rem;
    }
    .bg-dark{
        background-color: #000 !important;
    }
    .w-100{
        width: 640px !important;
    }
    .h-100{
        max-height: 150px !important;
    }
    .text-truncate{
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }
    .text-center{
        text-align: center;
    }
    .m-auto{
        margin: 0 auto;
    }
    .mt-ass{
        margin-top: 5rem;
    }
</style>
<body>
    <div>
        <div class="float-left">
            <img src="{{$_SERVER['DOCUMENT_ROOT'].'/imagens/logo.jpg'}}" alt="Logo" width="100">
            <!--<img src="{{URL::asset('imagens/logo.jpg')}}" class="img-fluid" alt="Logo" width="150">-->
        </div>
        <div class="float-right text-center mr-0">
            <h2><u>ASSOCIAÇÃO ASSISTENCIAL MEIMEI</u></h2>
            <span>
                Declaração de Utilidade Pública Municipal - Lei nº 1957 de 03/03/2004<br>
                Declaração de Utilidade Pública Estadual - Lei 13.052 de 06/06/2008<br>
                Declaraçao de Utilidade Pública Federal - Portaria nº 1.408 de 17/08/2007<br>
                Nº de inscrição CMDCA: 006/20006 - Número de inscrição CMAS: 00602005<br>
                Nº inscrição SEADS / PS - 5858/ 2007 - Nº Registro CNAS: R570/20<br>
                Telefone: (15) 3562 1068
            </span>
        </div>
        <div class="mr-0 clear-both mt-12 float-right">
            <img id="foto-aluno" src="{{$_SERVER['DOCUMENT_ROOT'].'/uploads/' . $aluno->foto_nome . '.' .$aluno->foto_tipo}}" width="175" class="mt-0">
            <!--<img id="foto-aluno" src="@if(isset($aluno->foto_nome)) {{URL::asset('uploads/' . $aluno->foto_nome . '.' .$aluno->foto_tipo)}} @else {{URL::asset('uploads/empty.png')}} @endif" width="250" class="img-thumbnail mt-5">-->
        </div>
        <div class="clear-both float-left">
            <h2 class="mt-12">FICHA INDIVIDUAL DO ALUNO</h2>
            <br>
            <p><b>Projeto: </b>{{$aluno->projeto}}</p>
            <p><b>Encaminhamento: </b>{{$aluno->encaminhamento}}</p>
            <br>
            <h3>DADOS DO ALUNO:</h3>
            <p><b>Nome Aluno: </b>{{$aluno->nome}}</p>
            <p><b>Data Nascimento: </b>{{\Carbon\Carbon::parse($aluno->data_nascimento)->format('d/m/Y')}}</p>
            <p><b>Série: </b>{{$aluno->serie}}</p>
            <p><b>Escola: </b>{{$aluno->escola}}</p>
            <p><b>Endereço: </b>{{$aluno->endereco}}</p>
            <p><b>Bairro: </b>{{$aluno->bairro}}</p>
            <p><b>Tel Contato: </b>{{$aluno->telefone}}</p>
            <p><b>Quantidades integrantes familiares: </b>{{$aluno->qtd_familiares}}</p>
            <p><b>NIS: </b>{{$aluno->nis}}</p>
            <p><b>Renda Familiar: </b>R$ {{$aluno->renda_familiar}}</p>
            <br>
            <h3><b>RESPONSÁVEIS:</b></h3>
            <p><b>Nome do pai: </b>{{$aluno->nome_pai}}</p>
            <p><b>Local de trabalho: </b>{{$aluno->local_trabalho_pai}}&nbsp;&nbsp;&nbsp;<b>Telefone: </b>{{$aluno->telefone_pai}}</p>
            <p><b>Nome mãe: </b>{{$aluno->nome_mae}}</p>
            <p><b>Local de trabalho: </b>{{$aluno->local_trabalho_mae}}&nbsp;&nbsp;&nbsp;<b>Telefone: </b>{{$aluno->telefone_mae}}</p>
            <br>
            <p><b>Data de início na entidade: </b>{{\Carbon\Carbon::parse($aluno->data_inicio)->format('d/m/Y')}}</p>
            <br>
            <p><b>Data de desligamento: </b>@isset($aluno->data_desligamento) {{\Carbon\Carbon::parse($aluno->data_desligamento)->format('d/m/Y')}} @endisset</p>
            <br>
            <p><b>Motivo de desligamento: </b>{{$aluno->motivo_desligamento}}</p>
            <br>
            <div class="w-100 h-100">
                <p><b>Obs:</b></p>
                <br>
                <span>{{$aluno->observacao}}</span>
            </div>
            <div class="w-100 h-100 text-center mt-ass">
                <span>_________________________________________________________</span>
                <br>
                <span>Assinatura do(a) responsável</span>
            </div>
        </div>
    </div>
</body>

</html>
