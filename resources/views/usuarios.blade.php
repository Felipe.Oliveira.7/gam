<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="robots" content="noindex,nofollow,noarchive,nosnippet,noodp,notranslate,noimageindex" />
    <meta name="csrf-token" content="<?php echo csrf_token(); ?>">
    <title>Sistema de Cadastros de Alunos GAM</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
</head>
<style>
    .title{
        margin: 20px;
    }
    .row>*{
        padding-right: 0 !important;
        padding-left: 0 !important;
    }
    .row{
        margin: 5rem !important;
    }
    .top-bar{
        margin: 0 !important;
        padding-right: 1.5rem;
    }
    .search{
        margin: 1rem 5rem -4rem 5rem;
    }
    .icon-bar{
        font-size: 42px;
    }
    .badge{
        --bs-badge-font-size: 1rem !important;
    }
    .bg-body{
        background-color: #EEE !important;
    }
</style>
<body class="">
    <div class="container-fluid p-0">

        <div class="row top-bar border-bottom bg-light bg-gradient">
            <nav class="navbar">
                <div class="col-4">
                    <div class="container-fluid">
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>
                </div>
                <div class="col-4 text-center mt-1">
                    <a href="/home">
                        <img src="{{URL::asset('imagens/logo.jpg')}}" alt="Logo" class="img-thumbnail" width="52">
                    </a>
                </div>
                <div class="col-4 pt-3 pb-3 d-flex flex-row-reverse">
                    <a href="/logout" class="btn btn-outline-danger">Sair</a>
                </div>
            </nav>
        </div>

        <div class="collapse" id="navbarToggleExternalContent">
            <div class="bg-secondary p-4">
                <h6 class="text-white"><a href="/home" class="link-light text-decoration-none">Home</a></h6>
                <h6 class="text-white">Usuários</h6>
                <h6 class="text-white"><a href="/alunos" class="link-light text-decoration-none">Alunos</a></h6>
                <h6 class="text-white"><a href="/relatorio" class="link-light text-decoration-none">Relatórios</a></h6>
            </div>
        </div>

        <div class="container-fluid mt-2">
            <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/home">
                    Home
                    </a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    Gerenciamentos de Usuários
                </li>
            </ol>
            </nav>
        </div>

        <div class="container-fluid">
            <h5 class="badge bg-light text-dark">
                GERENCIAMENTO DE USUÁRIOS
            </h5>
        </div>

        <form id="formPesquisaUsuario" action="{{route('usuario.pesquisar')}}" method="GET" >
            @csrf
            <div class="search">
                <div class="form-group input-group rounded">
                    <span class="input-group-text border-0 material-symbols-outlined">
                    search
                    </span>

                    <input onchange="pesquisarUsuario();" id="pesquisar" name="search" type="search" class="form-control rounded" placeholder="  Digite o nome ou email do usuário e tecle [enter]" aria-label="Search" aria-describedby="search-addon" value="{{$search ?? ''}}">
                </div>
            </div>
        </form>

        <div class="row">
            <div class="table-responsive">
                <table id="tabelaUsuarios" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                        <th width="100px" scope="col" class="text-center">Ações</th>
                        <th width="40%" scope="col">Nome</th>
                        <th width="35%" scope="col">E-mail</th>
                        <th width="160px" scope="col">Cadastrado em</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <input type="hidden" class="dados" value="{{ $user }}" >
                            <td scope="row">
                                <div class="text-center">
                                    <button id="btnNovo" type="button" class="btn btn-sm btn-light p-0 bg-secondary bg-gradient" data-tt-toggle="tooltip" data-bs-placement="top" title="Novo">
                                        <span class="material-symbols-outlined p-1 text-white fs-6">add_circle</span>
                                    </button>
                                    <button id="btnDeletar" type="button" class="btn btn-sm btn-light p-0 bg-secondary bg-gradient" data-tt-toggle="tooltip"  data-bs-placement="top" title="Deletar">
                                        <span class="material-symbols-outlined p-1 text-white fs-6">do_not_disturb_on</span>
                                    </button>
                                    <button id="btnEditar" type="button" class="btn btn-sm btn-light p-0 bg-secondary bg-gradient" data-tt-toggle="tooltip" data-bs-placement="top" title="Editar">
                                        <span class="material-symbols-outlined p-1 text-white fs-6">edit</span>
                                    </button>
                                </div>
                            </td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {!! $users->withQueryString()->links('pagination::bootstrap-5') !!}

                <div class="modal fade" id="modalInfo" tabindex="-1" aria-labelledby="titleInfo" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <span class="material-symbols-outlined"></span>
                                &nbsp;&nbsp;&nbsp;
                                <h5 class="modal-title" id="titleInfo"></h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="m-4">
                                    <form class="g-3 needs-validation" id="formUser" novalidate>
                                        @csrf
                                        <input type="hidden" id="id" name="id">

                                        <div class="alert alert-success alert-dismissible fade show" id="mensagem-sucesso" role="alert"></div>

                                        <div class="alert alert-danger alert-dismissible fade show" id="mensagem-erro" role="alert"></div>

                                        <div class="mb-2 col-md-10">
                                            <label for="nome" class="form-label">*Nome:</label>
                                            <input onkeydown="validate();" type="text" class="form-control" id="name" name="name" maxlength="150" required>
                                        </div>
                                        <div class="mb-2 col-md-10">
                                            <label for="email" class="form-label">*E-mail:</label>
                                            <input onkeydown="validate();" type="email" class="form-control" id="email" name="email" maxlength="150" aria-describedby="emailHelp" required>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="senha" class="col-form-label">*Senha:&nbsp;&nbsp;</label>
                                            <input onkeydown="validate();" onkeyup="validatePassword();" name="password" type="password" id="password" class="form-control" aria-describedby="passwordHelpInline" minlength="6" maxlength="12" required>
                                        </div>
                                        <div class="mb-2 col-md-12">
                                            <span id="texto-senha" class="form-text">
                                                A senha deve conter de 6 a 12 caracteres com letras maiúsculas, minúsculas, números e caracteres especiais (!@#$%^&*).
                                            </span>
                                        </div>
                                        <div class="mb-4 col-md-5">
                                            <label for="confirme_senha" class="form-label">*Confirmar senha:</label>
                                            <input onkeydown="validate();" onkeyup="validatePassword();" type="password" class="form-control" id="confirm_password" name="confirm_password" minlength="6" maxlength="12" required>
                                        </div>
                                        <button type="submit" class="btn btn-success" id="btnSalvar">Salvar</button>
                                        &nbsp;&nbsp;
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="modalDel" tabindex="-1" aria-labelledby="titleDel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <span class="material-symbols-outlined">do_not_disturb_on</span>
                                &nbsp;&nbsp;&nbsp;
                                <h5 class="modal-title" id="titleDel">Deletar Usuário</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-success alert-dismissible fade show" id="mensagem-sucesso-delete" role="alert"></div>
                                <div class="alert alert-danger alert-dismissible fade show" id="mensagem-erro-delete" role="alert"></div>
                                <span>Deseja realmente deletar esse usuário?</span>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Não</button>
                                <button type="button" class="btn btn-danger" onclick="deletarUsuario();">Sim</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script>

        $(document).ready(function () {

            $("#btnSalvar").addClass('disabled');

            var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-tt-toggle="tooltip"]'))
            var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                return new bootstrap.Tooltip(tooltipTriggerEl)
            });

            var forms = document.querySelectorAll('.needs-validation')

            Array.prototype.slice.call(forms)
                .forEach(function (form) {

                form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
                });

            // LÓGICA DE SUBMIT DO MODAL DE CADASTRO E EDIÇÃO
            $("#mensagem-sucesso").hide();
            $("#mensagem-erro").hide();
            $("#mensagem-sucesso-delete").hide();
            $("#mensagem-erro-delete").hide();

            $("#formUser").submit(function(event){

                if(validate()) {

                    event.preventDefault();
                    var id = $("#id").val();

                    if(id.length == 0) {
                        cadastrarUsuario(this);
                    } else {
                        editarUsuario(this);
                    }
                }

                return false;
            });

            //AÇÕES DOS BOTÕES CADASTRAR, EDITAR E DELETAR DA TABELA
            $('table').on('click', 'button', function(event) {

                var data = JSON.parse($(this).closest('tr').find(".dados").val());

                if(event.currentTarget.id =='btnNovo') {

                    $("#titleInfo").prev().text('add_circle');
                    $("#titleInfo").text("Cadastrar Usuário");

                    limparCampos();
                    $("#modalInfo").modal('show');
                }

                if((event.currentTarget.id == 'btnEditar')){

                    $("#titleInfo").prev().text('edit');
                    $("#titleInfo").text("Editar Usuário");

                    limparCampos();

                    $("#id").val(data.id);
                    $("#name").val(data.name);
                    $("#email").val(data.email);

                    $("#modalInfo").modal('show');
                }

                if(event.currentTarget.id == 'btnDeletar') {
                    $("#id").val(data.id);
                    $("#modalDel").modal('show');
                }
            });
        });

        function cadastrarUsuario(form) {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{route('usuario.novo')}}",
                data: $(form).serialize(),
                dataType: "json",
                type: "POST",
                success: function (data) {

                    $("#mensagem-sucesso").hide();
                    $("#mensagem-erro").hide();

                    limparCampos();

                    if(data.success){

                        var form = document.getElementById("formUser");
                        form.classList.remove('was-validated');

                        $("#mensagem-sucesso span").remove();
                        $("#mensagem-sucesso").append("<span>" + data.message + "</span>");
                        $("#mensagem-sucesso").show().delay(3200).fadeOut();

                        window.setTimeout(function(){
                            window.location.reload();
                        }, 3200);
                    }else{
                        $("#mensagem-erro").append("<span>" + data.message + "</span>");
                        $("#mensagem-erro").show();
                    }
                }
            });
        }

        function editarUsuario(form) {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{route('usuario.editar')}}",
                data: $(form).serialize(),
                dataType: "json",
                type: "PUT",
                success: function (data) {

                    $("#mensagem-sucesso").hide();
                    $("#mensagem-erro").hide();

                    limparCampos();

                    if(data.success){

                        var form = document.getElementById("formUser");
                        form.classList.remove('was-validated');

                        $("#mensagem-sucesso span").remove();
                        $("#mensagem-sucesso").append("<span>" + data.message + "</span>");
                        $("#mensagem-sucesso").show().delay(3200).fadeOut();

                        window.setTimeout(function(){
                            window.location.reload();
                        }, 3200);
                    }else{
                        $("#mensagem-erro").append("<span>" + data.message + "</span>");
                        $("#mensagem-erro").show();
                    }
                }
            });
        }

        function deletarUsuario() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{route('usuario.deletar')}}",
                data: {
                    id: $("#id").val(),
                },
                dataType: "json",
                type: "DELETE",
                success: function (data) {

                    $("#mensagem-sucesso-delete").hide();
                    $("#mensagem-erro-delete").hide();

                    if(data.success){
                        $("#mensagem-sucesso-delete span").remove();
                        $("#mensagem-sucesso-delete").append("<span>" + data.message + "</span>");
                        $("#mensagem-sucesso-delete").show().delay(3200).fadeOut();

                        window.setTimeout(function(){
                            window.location.reload();
                        }, 3200);
                    }else{
                        $("#mensagem-erro-delete").append("<span>" + data.message + "</span>");
                        $("#mensagem-erro-delete").show();
                    }
                }
            });
        }

        function validatePassword() {

            var response = false;
            var password = $("#password").val();
            var confirm_password = $("#confirm_password").val();
            var regex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,12})");

            var domPassword = document.getElementById("password");
            domPassword.classList.remove('was-validated');

            var domConfirmPassword = document.getElementById("confirm_password");
            domConfirmPassword.classList.remove('was-validated');

            $("#password").removeClass('was-validated');
            $("#confirm_password").removeClass('was-validated');
            $("#password").removeClass('is-valid');
            $("#confirm_password").removeClass('is-valid');
            $("#password").addClass('is-invalid');
            $("#confirm_password").addClass('is-invalid')

            if((password.length > 0) && confirm_password.length > 0){
                if(password === confirm_password) {
                    if(regex.test(password) && regex.test(confirm_password)){

                        $("#password").removeClass('is-invalid');
                        $("#confirm_password").removeClass('is-invalid');

                        $("#password").addClass('is-valid');
                        $("#confirm_password").addClass('is-valid');

                        response = true;
                    }
                }
            }

            return response;
        }

        function validate() {

            var checkName = false;
            var checkPassword = false;
            var checkEmail = false;
            var response = false;

            $("#name").removeClass('is-valid');
            $("#email").removeClass('is-valid');
            $("#name").addClass('is-invalid');
            $("#email").addClass('is-invalid');

            var name = $("#name").val();
            var email = $("#email").val();
            var regexEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

            if(name.length > 0 ){
                $("#name").removeClass('is-invalid');
                $("#name").addClass('is-valid');
                checkName = true;
            }

            if(email.length > 0) {
                if(regexEmail.test(email)){
                    $("#email").removeClass('is-invalid');
                    $("#email").addClass('is-valid');
                    checkEmail = true;
                }
            }

            checkPassword = validatePassword();

            if(checkName && checkEmail && checkPassword) {
                $("#btnSalvar").removeClass('disabled');
                response = true;
            }else{
                $("#btnSalvar").addClass('disabled');
            }

            return response;
        }

        function limparCampos() {

            $("#id").val("");
            $("#name").val("");
            $("#email").val("");
            $("#password").val("");
            $("#confirm_password").val("");
        }

        function pesquisarUsuario() {

            $("#formPesquisaUsuario").submit(function(event){

                var response = false;

                if($('#pesquisar').val() != ''){
                    response = true;
                }

                return response;
            });
        }

        function populaTabela(dados) {

            $('#tabelaUsuarios tbody tr').remove();
            var html = "";
            for(var i = 0; i < dados.length; i++){

                html += '<tr>'+
                            "<input type='hidden' class='dados' value='" + JSON.stringify(dados[i]) + "' >"+
                            "<td scope='row'>" +
                                "<div class='text-center'>" +
                                    "<button id='btnNovo' type='button' class='btn btn-sm btn-light p-0 bg-secondary bg-gradient' data-tt-toggle='tooltip' data-bs-placement='top' title='Novo'>" +
                                        "<span class='material-symbols-outlined p-1 text-white fs-6'>add_circle</span>" +
                                    "</button>" +
                                    "<button id='btnDeletar' type='button' class='btn btn-sm btn-light p-0 bg-secondary bg-gradient' data-tt-toggle='tooltip'  data-bs-placement='top' title='Deletar'>" +
                                        "<span class='material-symbols-outlined p-1 text-white fs-6'>do_not_disturb_on</span>" +
                                    "</button>" +
                                    "<button id='btnEditar' type='button' class='btn btn-sm btn-light p-0 bg-secondary bg-gradient' data-tt-toggle='tooltip' data-bs-placement='top' title='Editar'>" +
                                        "<span class='material-symbols-outlined p-1 text-white fs-6'>edit</span>" +
                                    "</button>" +
                                "</div>" +
                            "</td>" +
                            '<td>' + dados[i].name + '</td>' +
                            '<td>' + dados[i].email + '</td>' +
                            '<td>' + dados[i].created_at + '</td>' +
                        '</tr>';
            }

            $('#tabelaUsuarios tbody').append(html);
        }

    </script>
</body>
</html>
