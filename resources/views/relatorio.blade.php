<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="robots" content="noindex,nofollow,noarchive,nosnippet,noodp,notranslate,noimageindex" />
    <meta name="csrf-token" content="<?php echo csrf_token(); ?>">
    <title>Sistema de Cadastros de Alunos GAM</title>
</head>
<style>
    body{
        background-color: #fff;
        padding: 2rem;
    }
    h2{
        font-size: 16px;
    }
    h3{
        font-size: 14px;
    }
    span{
        font-size: 12px;
    }
    p, th{
        font-size: 13px;
        margin-bottom: -0.5rem;
    }
    .float-right{
        float: right;
    }
    .float-left{
        float: left;
    }
    .text-center{
        text-align: center;
    }
    .clear-both{
        clear: both;
    }
    .mr-0{
        margin-right: 0rem;
    }
    .mt-0{
        margin-top: 0rem;
    }
    .mt-12{
        margin-top: 12rem;
    }
    .bg-dark{
        background-color: #000 !important;
    }
    .w-100{
        width: 640px !important;
    }
    .h-100{
        max-height: 150px !important;
    }
    .text-truncate{
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }
    .text-center{
        text-align: center;
    }
    .m-auto{
        margin: 0 auto;
    }
    .text-left{
        text-align: left;
    }
    .mt-alunos{
        margin-top: 2rem;
    }
</style>
<body>
    <div>
        <div class="float-left">
            <img src="{{$_SERVER['DOCUMENT_ROOT'].'/imagens/logo.jpg'}}" alt="Logo" width="100">
            <!--<img src="{{URL::asset('imagens/logo.jpg')}}" class="img-fluid" alt="Logo" width="150">-->
        </div>
        <div class="float-right text-center mr-0">
            <h2><u>ASSOCIAÇÃO ASSISTENCIAL MEIMEI</u></h2>
            <span>
                Declaração de Utilidade Pública Municipal - Lei nº 1957 de 03/03/2004<br>
                Declaração de Utilidade Pública Estadual - Lei 13.052 de 06/06/2008<br>
                Declaraçao de Utilidade Pública Federal - Portaria nº 1.408 de 17/08/2007<br>
                Nº de inscrição CMDCA: 006/20006 - Número de inscrição CMAS: 00602005<br>
                Nº inscrição SEADS / PS - 5858/ 2007 - Nº Registro CNAS: R570/20<br>
                Telefone: (15) 3562 1068
            </span>
        </div>
        <div class="clear-both float-left">
            <h2 class="mt-12 text-center">RELATÓRIO DE ALUNOS</h2>
            <br>
            <h3>FILTROS:</h3>
            <p><b>Data de Geração: </b>{{\Carbon\Carbon::now()->format('d/m/Y H:i')}}</p>
            <p><b>Período: </b>{{$filtros['periodo_de']}}&nbsp;&nbsp;<b> ate: </b>{{$filtros['periodo_ate']}}</p>
            <p><b>Status: </b>{{$filtros['status']}}</p>
            <p><b>Projeto: </b>{{$filtros['projeto']}}</p>
            <p><b>Encaminhamento: </b>{{$filtros['encaminhamento']}}</p>
            <p><b>Bairro: </b>{{$filtros['bairro']}}</p>
            <p><b>Escola: </b>{{$filtros['escola']}}</p>
            <p><b>Idade: </b>{{$filtros['idade_de']}}&nbsp;&nbsp;<b> e </b>{{$filtros['idade_ate']}}</p>
            <p><b>Ordenado por: </b>{{$filtros['ordenar']}}</p>
            <p><b>Em ordem: </b>@if($filtros['em_ordem'] == 'asc') {{ 'crescente' }} @else {{ 'decrescente' }} @endif</p>
            <br>
            <p><b>Total de Alunos: </b>{{$total_alunos}}</p>
            <div class="w-100 h-100 mt-alunos">
                <table class="">
                    <thead>
                        <tr>
                        <th width="400px" scope="col" class="text-left">Nome</th>
                        <th width="160px" scope="col" class="text-left">Idade</th>
                        <th width="110px" scope="col" class="text-left">Data início</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($alunos as $aluno)
                        <tr>
                            <td><span class="">{{$aluno->nome}}</span></td>
                            <td><span class="">{{\Carbon\Carbon::parse($aluno->data_nascimento)->age}} anos</span></td>
                            <td><span>{{\Carbon\Carbon::parse($aluno->data_inicio)->format('d/m/Y')}}</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>
