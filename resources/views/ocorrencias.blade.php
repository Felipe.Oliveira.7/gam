<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="robots" content="noindex,nofollow,noarchive,nosnippet,noodp,notranslate,noimageindex" />
    <meta name="csrf-token" content="<?php echo csrf_token(); ?>">
    <title>Sistema de Cadastros de Alunos GAM</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
</head>
<style>
    body{
        background-color: #fff;
        padding: 2rem;
    }
    h2{
        font-size: 16px;
    }
    span{
        font-size: 12px;
    }
    p{
        font-size: 14px;
        margin-bottom: 0rem;
    }
    .float-right{
        float: right;
    }
    .float-left{
        float: left;
    }
    .text-center{
        text-align: center;
    }
    .clear-both{
        clear: both;
    }
    .mr-0{
        margin-right: 0rem;
    }
    td{
        border: 4px solid #000 !important;
        padding: 1rem;
    }
    tr{
    }
</style>
<body>
    <div>
        <div class="float-left">
            <!--<img src="{{$_SERVER['DOCUMENT_ROOT'].'/imagens/logo.jpg'}}" alt="Logo" width="100">-->
            <img src="{{URL::asset('imagens/logo.jpg')}}" class="img-fluid" alt="Logo" width="150">
        </div>
        <div class="float-right text-center mr-0">
            <h2><u>ASSOCIAÇÃO ASSISTENCIAL MEIMEI</u></h2>
            <span>
                Declaração de Utilidade Pública Municipal - Lei nº 1957 de 03/03/2004<br>
                Declaração de Utilidade Pública Estadual - Lei 13.052 de 06/06/2008<br>
                Declaraçao de Utilidade Pública Federal - Portaria nº 1.408 de 17/08/2007<br>
                Nº de inscrição CMDCA: 006/20006 - Número de inscrição CMAS: 00602005<br>
                Nº inscrição SEADS / PS - 5858/ 2007 - Nº Registro CNAS: R570/20<br>
                Nº de cadastro CRCE 0544/2012 Cebas: 104/2015<br>
                Telefone: (15) 3562 1068<br>
                https://gamitabera.wordpress.com/<br>
            </span>
        </div>
        <div class="clear-both float-left">
            <h2 class="mt-5 text-center">FICHA DE ACOMPANHAMENTO DA CRIANÇA OU ADOLESCENTE</h2>
            <br>
            <p><b>Nome Aluno: </b>{{$aluno->nome}}</p>
            <p><b>Nome Responsável: </b>{{$aluno->nome_mae}}</p>
            <p><b>Endereço: </b>{{$aluno->endereco}}</p>
            <p><b>Bairro: </b>{{$aluno->bairro}}</p>
            <p><b>Contato: </b>{{$aluno->telefone}}</p>
            <p><b>Idade: </b>{{\Carbon\Carbon::parse($aluno->data_nascimento)->age}} anos</p>
            <p><b>Início das atividades: </b>{{\Carbon\Carbon::parse($aluno->data_inicio)->format('d/m/Y')}}</p>
            <br>
            <p>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;) Desligamento</p>
            <br>
            <p><b>Nas observações é necessário conter qual encaminhamento e/ou providências:</b></p>

            <br>

             @if(count($ocorrencias) > 0)


                <table>
                    @foreach ($ocorrencias as $ocorrencia)
                        <tr>
                            <td>{{ \Carbon\Carbon::parse($ocorrencia->data_ocorrencia)->format('d/m/Y')}} </td>
                            <td><span>{{ $ocorrencia->observacao }}</span></td>
                            <td>
                                <p>Informado(a) ao responsável</p>
                                <br class="">
                                <p>(&nbsp;&nbsp;&nbsp;)sim (&nbsp;&nbsp;&nbsp;)não</p>
                            </td>
                        </tr>
                    @endforeach
                </table>
                @else

                    <p class="fw-normal text-center text-secondary mb-0"><i>Não existe ocorrências para este aluno.</i></p>

                @endif
        </div>
    </div>
</body>

</html>
