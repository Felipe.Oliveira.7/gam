<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="robots" content="noindex,nofollow,noarchive,nosnippet,noodp,notranslate,noimageindex" />
    <meta name="csrf-token" content="<?php echo csrf_token(); ?>">
    <title>Sistema de Cadastros de Alunos GAM</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
</head>
<style>
    .title{
        margin: 20px;
    }
    .row>*{
        padding-right: 0 !important;
        padding-left: 0 !important;
    }
    .row{
        margin: 2rem !important;
    }
    .top-bar{
        margin: 0 !important;
        padding-right: 1.5rem;
    }
    .search{
        margin: 3rem 5rem -4rem 5rem;
    }
    .icon-bar{
        font-size: 42px;
    }
    .badge{
        --bs-badge-font-size: 1rem !important;
    }
    .btn-actions{
        display: flex;
        align-items: center;
        justify-content: center;
    }
</style>
<body>
    <div class="container-fluid p-0">

        <div class="row top-bar border-bottom bg-light bg-gradient">
            <nav class="navbar">
                <div class="col-4">
                    <div class="container-fluid">
                            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                    </div>
                </div>
                <div class="col-4 text-center mt-1">
                    <a href="/home">
                        <img src="{{URL::asset('imagens/logo.jpg')}}" alt="Logo" class="img-thumbnail" width="52">
                    </a>
                </div>
                <div class="col-4 pt-3 pb-3 d-flex flex-row-reverse">
                    <a href="/logout" class="btn btn-outline-danger">Sair</a>
                </div>
            </nav>
        </div>

        <div class="collapse" id="navbarToggleExternalContent">
            <div class="bg-secondary p-4">
                <h6 class="text-white"><a href="/home" class="link-light text-decoration-none">Home</a></h6>
                <h6 class="text-white"><a href="/usuarios" class="link-light text-decoration-none">Usuários</a></h6>
                <h6 class="text-white"><a href="/alunos" class="link-light text-decoration-none">Alunos</a></h6>
                <h6 class="text-white"><a href="/relatorio" class="link-light text-decoration-none">Relatórios</a></h6>
            </div>
        </div>

        <div class="container-fluid mt-2">
            <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/home">
                    Home
                    </a>
                </li>
                <li class="breadcrumb-item" aria-current="page">
                    <a href="/alunos">
                        Gerenciamento de Alunos
                    </a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    Ocorrências
                </li>
            </ol>
            </nav>
        </div>

        <div class="container-fluid mb-3">
            <h5 class="badge bg-light text-dark">
                OCORRÊNCIAS DO ALUNO {{ $aluno->nome }}
            </h5>
        </div>

        <div class="container p-0 mb-3 d-flex flex-row-reverse">
            <button type="button" class="btn btn-primary" id="btnNovaOcorrencia">
                Nova Ocorrência
            </button>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-secondary" id="btnImprimirOcorrencia">
                Imprimir
            </button>
        </div>

        <div class="container p-0">
            <div class="alert alert-success alert-dismissible fade show" id="mensagem-sucesso" role="alert"></div>
            <div class="alert alert-danger alert-dismissible fade show" id="mensagem-erro" role="alert"></div>
        </div>

        <div class="container border p-4 mb-4">

            <div class="list-group">

                @if($ocorrencias->count() > 0)
                    @foreach ($ocorrencias as $ocorrencia)

                        <a href="#" class="list-group-item list-group-item-action" aria-current="true">
                            <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">{{ $ocorrencia->tipo }}</h5>
                            <small>{{ $ocorrencia->data_ocorrencia->toFormattedDateString() }}</small>
                            </div>
                            <p class="mb-1">{{ $ocorrencia->observacao }}</p>
                            <small>{{ $ocorrencia->educador }}</small>
                        </a>

                    @endforeach
                @else

                    <p class="fw-normal text-center text-secondary mb-0"><i>Não existe ocorrências para este aluno.</i></p>

                @endif

            </div>
        </div>

        <div class="modal fade" id="modalNovaOcorrencia" tabindex="-1" aria-labelledby="titleOcorrencia" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <span class="material-symbols-outlined">calendar_add_on</span>
                        &nbsp;&nbsp;&nbsp;
                        <h5 class="modal-title" id="titleFicha">Nova Ocorrência</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <form id="formNovaOcorrencia" class="g-3 needs-validation" novalidate>
                            @csrf

                            <input type="hidden" name="aluno_id" id="aluno_id" value="{{$aluno->id ?? ''}}">

                            <div class="row">
                                <div class="col-sm-4">
                                    <input min="2000-01-01" max="{{ now()->toDateString('Y-m-d') }}" type="date" class="form-control" id="data" name="data" placeholder="99/99/9999" required>
                                </div>
                            </div>

                            <div class="row">
                                <select class="form-select" id="educador" name="educador" aria-label="Default select example" required>
                                    <option value="" selected>Selecione o educador...</option>
                                    <option value="Ademar">Ademar</option>
                                    <option value="Amanda">Amanda</option>
                                    <option value="Nataly">Nataly</option>
                                    <option value="Pedro">Pedro</option>
                                    <option value="Elisiane">Elisiane</option>
                                </select>
                            </div>

                            <div class="row">
                                <select class="form-select" id="tipo" name="tipo" aria-label="Default select example" required>
                                    <option value="" selected>Selecione o tipo...</option>
                                    <option value="Advertencia">Advertência</option>
                                    <option value="Acidente">Acidente</option>
                                    <option value="Imprevisto">Imprevisto</option>
                                </select>
                            </div>

                            <div class="row">
                                <textarea class="form-control" id="obs" name="obs" rows="4" placeholder="Observação..."></textarea>
                            </div>

                            <input type="hidden" id="idOcorrencia">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success" id="btnSalvar">Salvar</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
    <script src="{{URL::asset('js/mensagens.js')}}" type="text/javascript"></script>
    <script>

    $(document).ready(function(){

        var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-tt-toggle="tooltip"]'))
        var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
            return new bootstrap.Tooltip(tooltipTriggerEl)
        })

        var forms = document.querySelectorAll('.needs-validation')

        Array.prototype.slice.call(forms)
            .forEach(function (form) {

            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
            });

            $('#mensagem-sucesso').hide();
            $('#mensagem-erro').hide();

            $("#formNovaOcorrencia").submit(function(event){

                if($("#formNovaOcorrencia").valid()) {

                    event.preventDefault();

                    var id = $("#id").val();

                    cadastrarOcorrencia(this)
                }

                return false;
            });


            $("#formNovaOcorrencia").off().submit(function(event){

                if($("#formNovaOcorrencia").valid()) {

                    event.preventDefault();

                    cadastrarOcorrencia(this);
                }
                return false;
            });

        });

        $("#btnImprimirOcorrencia").click(function(event){

            var url = '{{ route("ocorrencia.imprimir", $aluno->id) }}';
            window.open(url, '_blank');
        });

        $("#btnNovaOcorrencia").click(function(event){

            $("#modalNovaOcorrencia").modal('show');
        });

        function cadastrarOcorrencia(form) {

            var formData = $(form).serialize();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{route('ocorrencia.nova', $aluno->id)}}",
                data: formData,
                dataType: 'json',
                type: "POST",
                success: function (data) {

                    $("#mensagem-sucesso").hide();
                    $("#mensagem-erro").hide();

                    if(data.success){

                        $("#idOcorrencia").val(data.id);

                        limparCampos();
                        $("#modalNovaOcorrencia").modal('hide');

                        var form = document.getElementById("formNovaOcorrencia");
                        form.classList.remove('was-validated');

                        window.setTimeout(function(){

                            $("#mensagem-sucesso span").remove();
                            $("#mensagem-sucesso").append("<span>" + data.message + "</span>");
                            $("#mensagem-sucesso").show().delay(5400).fadeOut();


                        }, 600);

                        window.setTimeout(function(){
                            location.reload();
                        }, 5400);

                    } else {

                        window.setTimeout(function(){
                            $("#mensagem-erro span").remove();
                            $("#mensagem-erro").append("<span>" + data.message + "</span>");
                            $("#mensagem-erro").show();
                        }, 600);
                    }
                }
            });
        }

        function limparCampos() {

            $("#id").val("");
            $("#data").val("");
            $("#educador").val("").change();
            $("#tipo").val("").change();
            $("#obs").val("").change();
        }

    </script>
</body>
</html>
