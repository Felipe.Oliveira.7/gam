<?php
namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class UserController extends Controller
{
    // Total de itens da paginação
    const TOTAL_PAGINATION = 7;

    public function index()
    {
        $users = User::paginate(self::TOTAL_PAGINATION);

        return view('usuarios', ['users' => $users]);
    }

    public function create(Request $request)
    {
        try{
            $response['message'] = "Usuário cadastrado com sucesso!";
            $response['success'] = true;
            $params = $request->all();

            $user = [
                'name' => $params['name'],
                'email' => $params['email'],
                'email_verified_at' => now(),
                'password' => md5($params['password']),
                'created_at' => now(),
                'updated_at' => now()
            ];

            User::create($user);
        } catch(\Exception $error) {
            $response['message'] = $error->getMessage();
            $response['success'] = false;
        }

        return json_encode($response);
    }

    public function update(Request $request)
    {
        try {
            $response['message'] = "Usuário editado com sucesso!";
            $response['success'] = true;
            $params = $request->all();

            if(!$params['id']){
                throw("ID do usuário não existe!");
            }

            $user = User::find($params['id']);

            $user->name = $params['name'];
            $user->email= $params['email'];
            $user->email_verified_at = now();
            $user->password = md5($params['password']);
            $user->updated_at = now();

            $user->save();
        } catch (\Exception $error) {
            $response['message'] = $error->getMessage();
            $response['success'] = false;
        }

        return json_encode($response);
    }

    public function delete(Request $request)
    {
        try {
            $response['message'] = "Usuário deletado com sucesso!";
            $response['success'] = true;
            $params = $request->all();

            if (!$params['id']) {
                throw ("ID do usuário não existe!");
            }

            $user = User::find($params['id']);

            $user->delete();
        } catch (\Exception $error) {
            $response['message'] = "Falha ao tentar deletar usuário!";
            $response['success'] = false;
        }

        return json_encode($response);
    }

    public function login()
    {
        if(!Auth::check()) {
            return view('welcome');
        }else{
            return redirect('home');
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }

    public function auth(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'senha' => 'required'
        ],[
            'email.required' => 'E-mail é obrigatório!',
            'senha.required' => 'Senha é obrigatória!'
        ]);

        $user = User::where('email', $request->email)
            ->where('password', md5($request->senha))
            ->first();

        if($user){
            if (($request->email == $user->email) && (md5($request->senha) == $user->password)) {
                Auth::login($user, true);
                return redirect()->intended('home');
            }
        }

        return redirect()->back()->with('danger','Email ou senha inválido(os)!');
    }

    public function search(Request $request) {
        try {
            $response['message'] = "Usuários encontrados!";
            $response['success'] = true;
            $response['data'] = [];

            $params = $request->all();

            $users = User::query()
            ->where('name','like','%'. $params['search'].'%')
            ->orWhere('email','like','%'. $params['search'].'%')
            ->paginate(self::TOTAL_PAGINATION);

            return view('usuarios', ['users' => $users, 'search' => $params['search']]);
        } catch (\Exception $error) {
            $response['message'] = $error->getMessage();
            $response['success'] = false;
        }

        return json_encode($response);
    }
}
