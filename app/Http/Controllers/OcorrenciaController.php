<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ocorrencia;
use App\Models\Aluno;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade\Pdf;

class OcorrenciaController extends Controller
{
    public function get(Request $request, $id)
    {
        if (!$id) {
            return response([
                'error' => true,
                'error-msg' => 'Página não encontrada!'
            ], 404);
        }

        $aluno = Aluno::find($id);
        $ocorrencias = Ocorrencia::where('id_aluno', $id)->get();

        if (!$ocorrencias) {
            return response([
                'error' => true,
                'error-msg' => 'Página não encontrada!'
            ], 404);
        }

        return view('ocorrencia', ['ocorrencias' => $ocorrencias, 'aluno' => $aluno]);
    }

    public function create(Request $request, $id)
    {
        try {

            $response['message'] = "Ocorrência cadastrada com sucesso!";
            $response['success'] = true;
            $response['id'] = null;

            $params = $request->all();

            $encoding = mb_internal_encoding();

            if (!$id) {
                return response([
                    'error' => true,
                    'error-msg' => 'Página não encontrada!'
                ], 404);
            }

            $ocorrencia = [
                'id_aluno' => $id,
                'tipo' => mb_strtoupper($params['tipo'], $encoding),
                'educador' => $params['educador'],
                'data_ocorrencia' => Carbon::parse($params['data'])->format('Y-m-d'),
                'observacao' => $params['obs'],
                'created_at' => now(),
                'updated_at' => now()
            ];

            $dados = Ocorrencia::create($ocorrencia);

            $response['id'] = $dados->id;

        } catch (\Exception $error) {
            $response['message'] = "Falha ao tentar cadastrar ocorrência. Mensagem do erro: <br>" . $error->getMessage();
            $response['success'] = false;
        }

        return json_encode($response);
    }

    public function events(Request $request, $id)
    {
        try {
            if (!isset($id)) {
                return response([
                    'error' => true,
                    'error-msg' => 'ID do aluno não existe!'
                ], 404);
            }

            set_time_limit(300);

            $aluno = Aluno::find($id);

            if (!$aluno) {
                return response([
                    'error' => true,
                    'error-msg' => 'Página não encontrada!'
                ], 404);
            }

            $ocorrencias = Ocorrencia::where('id_aluno', $id)->get();

            //view()->share('ocorrencias', ['aluno' => $aluno, 'ocorrencias' => $ocorrencias]);
            //$pdf = Pdf::loadView('ocorrencias', ['aluno' => $aluno, 'ocorrencias' => $ocorrencias]);

            return view('ocorrencias',['aluno' => $aluno, 'ocorrencias' => $ocorrencias]);

            //return $pdf->stream();
        } catch (\Exception $error) {
            dd($error->getMessage());
        }
    }
}
