<?php
namespace App\Http\Controllers;

use App\Models\Aluno;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Exception;
use Barryvdh\DomPDF\Facade\Pdf;
class AlunoController extends Controller
{
    // Total de itens da paginação
    const TOTAL_PAGINATION = 7;

    public function index(Request $request)
    {
        $params = $request->all();
        $search = '';

        if(!isset($params['search'])){
            $alunos = Aluno::orderBy('data_inicio', 'desc')
                ->paginate(self::TOTAL_PAGINATION);
        }else{
            $search = $params['search'];
            $alunos = Aluno::query()
                ->where('nome', 'like', '%' . $params['search'] . '%')
                ->orWhere('nome_pai', 'like', '%' . $params['search'] . '%')
                ->orWhere('nome_mae', 'like', '%' . $params['search'] . '%')
                ->orWhere('escola', 'like', '%' . $params['search'] . '%')
                ->orWhere('projeto', 'like', '%' . $params['search'] . '%')
                ->orWhere('encaminhamento', 'like', '%' . $params['search'] . '%')
                ->orderBy('data_inicio','desc')
                ->paginate(self::TOTAL_PAGINATION);
        }

        return view('alunos',['alunos' => $alunos, 'search' => $search]);
    }

    public function get(Request $request, $id)
    {
        if(!$id){
            return response(['error' => true,
                'error-msg' => 'Página não encontrada!'], 404);
        }

        $aluno = Aluno::find($id);

        if(!$aluno){
            return response([
                'error' => true,
                'error-msg' => 'Página não encontrada!'
            ], 404);
        }

        return view('aluno',['aluno' => $aluno]);
    }

    public function new(Request $request)
    {
        return view('aluno');
    }

    public function create(Request $request)
    {
        try {

            $response['message'] = "Aluno cadastrado com sucesso!";
            $response['success'] = true;
            $response['id'] = null;

            $params = $request->all();

            $request->validate([
                'foto' => 'required|image|mimes:jpg,png,jpeg|max:4096|dimensions:min_width=100,min_height=100,max_width=2000,max_height=2000',
            ],$messages = [
                'mimes' => 'Extensão de imagem inválida. As extensões válidas são: JPG, JPEG e PNG',
                'max'   => 'A imagem não pode ser maior que 4 MB.',
                'dimensions' => 'Imagem fora das dimensões mínimas de 100x100, ou máximas 1000x1000.'
            ]);

            $foto_nome = time();
            $foto_tipo = $request->file('foto')->extension();
            $foto = $foto_nome . '.' . $foto_tipo;
            $request->file('foto')->move(public_path('uploads'), $foto);
            // $file = $request->file('foto')->storeAs('uploads', $foto);
            // $blob = base64_encode(addslashes(file_get_contents($file)));

            $encoding = mb_internal_encoding();

            $aluno = [
                'nome' => mb_strtoupper($params['nome'], $encoding),
                'serie' => (int) $params['serie'],
                'escola' => $params['escola'],
                'data_nascimento' => Carbon::parse($params['data_nascimento'])->format('Y-m-d'),
                'endereco' => mb_strtoupper($params['endereco'], $encoding),
                'numero' => ($params['numero'])?? $params['numero'],
                'bairro' => $params['bairro'],
                'telefone' => $params['telefone'],
                'qtd_familiares' => (int) $params['qtd_familiares'],
                'nis' => $params['nis'],
                'renda_familiar' => (float) $params['renda_familiar'],
                'nome_pai' => mb_strtoupper($params['nome_pai'], $encoding),
                'telefone_pai' => $params['telefone_pai'],
                'nome_mae' => mb_strtoupper($params['nome_mae'], $encoding),
                'telefone_mae' => $params['telefone_mae'],
                'local_trabalho_pai' => ($params['local_trabalho_pai']) ?? mb_strtoupper($params['local_trabalho_pai'], $encoding),
                'local_trabalho_mae' => ($params['local_trabalho_mae']) ?? mb_strtoupper($params['local_trabalho_mae'], $encoding),
                'data_inicio' => Carbon::parse($params['data_inicio'])->format('Y-m-d'),
                'projeto' => $params['projeto'],
                'encaminhamento' => $params['encaminhamento'],
                'data_desligamento' => (!empty($params['data_desligamento'])) ? Carbon::parse($params['data_desligamento'])->format('Y-m-d') : null,
                'motivo_desligamento' => $params['motivo_desligamento'] ?? null,
                'observacao' => $params['observacao'],
                'foto' => null,
                'foto_tipo' => $foto_tipo,
                'foto_nome' => $foto_nome,
                'created_at' => now(),
                'updated_at' => now()
            ];

            //dd($aluno);

            $dados = Aluno::create($aluno);

            $response['id'] = $dados->id;
        } catch (\Exception $error) {
            $response['message'] = "Falha ao tentar cadastrar aluno. Mensagem do erro: <br>".$error->getMessage();
            $response['success'] = false;
        }

        return json_encode($response);
    }

    public function delete(Request $request)
    {
        try {
            $response['message'] = "Aluno deletado com sucesso!";
            $response['success'] = true;
            $params = $request->all();

            if (!isset($params['id'])) {
                throw new Exception("ID do aluno não existe!");
            }

            $user = Aluno::find($params['id']);

            $user->delete();
        } catch (\Exception $error) {
            $response['message'] = "Falha ao tentar deletar aluno. Mensagem do erro: <br>" . $error->getMessage();
            $response['success'] = false;
        }

        return json_encode($response);
    }

    public function update(Request $request)
    {
        try {

            $response['message'] = "Aluno editado com sucesso!";
            $response['success'] = true;
            $params = $request->all();

            if (!isset($params['id'])) {
                throw new Exception("ID do aluno não existe!");
            }

            $response['id'] = $params['id'];
            $aluno = Aluno::find($params['id']);

            if($request->file('foto')) {

                $request->validate([
                    'foto' => 'required|image|mimes:jpg,png,jpeg|max:4096|dimensions:min_width=100,min_height=100,max_width=2000,max_height=2000',
                ], $messages = [
                    'mimes' => 'Extensão de imagem inválida. As extensões válidas são: JPG, JPEG e PNG',
                    'max'   => 'A imagem não pode ser maior que 4 MB.',
                    'dimensions' => 'Imagem fora das dimensões mínimas de 100x100, ou máximas 1000x1000.'
                ]);

                $foto_nome = time();
                $foto_tipo = $request->file('foto')->extension();
                $foto = $foto_nome . '.' . $foto_tipo;
                $request->file('foto')->move(public_path('uploads'), $foto);

                $aluno->foto = null;
                $aluno->foto_tipo = $foto_tipo;
                $aluno->foto_nome = $foto_nome;
            }

            $encoding = mb_internal_encoding();

            $aluno->nome = mb_strtoupper($params['nome'], $encoding);
            $aluno->serie = (int) $params['serie'];
            $aluno->escola = $params['escola'];
            $aluno->data_nascimento = Carbon::parse($params['data_nascimento'])->format('Y-m-d');
            $aluno->endereco = mb_strtoupper($params['endereco'], $encoding);
            $aluno->numero = ($params['numero']) ?? $params['numero'];
            $aluno->bairro = $params['bairro'];
            $aluno->telefone = $params['telefone'];
            $aluno->qtd_familiares = (int) $params['qtd_familiares'];
            $aluno->nis = $params['nis'];
            $aluno->renda_familiar = (float) $params['renda_familiar'];
            $aluno->nome_pai = mb_strtoupper($params['nome_pai'], $encoding);
            $aluno->telefone_pai = $params['telefone_pai'];
            $aluno->nome_mae = mb_strtoupper($params['nome_mae'], $encoding);
            $aluno->telefone_mae = $params['telefone_mae'];
            $aluno->local_trabalho_pai = ($params['local_trabalho_pai']) ?? mb_strtoupper($params['local_trabalho_pai'], $encoding);
            $aluno->local_trabalho_mae = ($params['local_trabalho_mae']) ?? mb_strtoupper($params['local_trabalho_mae'], $encoding);
            $aluno->data_inicio = Carbon::parse($params['data_inicio'])->format('Y-m-d');
            $aluno->projeto = $params['projeto'];
            $aluno->encaminhamento = $params['encaminhamento'];
            $aluno->data_desligamento = (!empty($params['data_desligamento'])) ? Carbon::parse($params['data_desligamento'])->format('Y-m-d') : null;
            $aluno->motivo_desligamento = $params['motivo_desligamento'] ?? null;
            $aluno->observacao = $params['observacao'];
            $aluno->updated_at = now();

            $aluno->save();
        } catch (\Exception $error) {
            $response['message'] = "Falha ao tentar editar aluno. Mensagem do erro: <br>" . $error->getMessage();
            $response['success'] = false;
        }

        return json_encode($response);
    }

    public function profile(Request $request, $id)
    {
        try {
            if (!isset($id)) {
                return response([
                    'error' => true,
                    'error-msg' => 'ID do aluno não existe!'
                ], 404);
            }

            $aluno = Aluno::find($id);

            if (!$aluno) {
                return response([
                    'error' => true,
                    'error-msg' => 'Página não encontrada!'
                ], 404);
            }



            //view()->share('ficha', ['aluno' => $aluno]);
            $pdf = Pdf::loadView('ficha', ['aluno' => $aluno]);
            //return $pdf->download('ficha_aluno_' . $id . '.pdf');
            return $pdf->stream();

            //return view('ficha',['aluno' => $aluno]);

        } catch (\Exception $error) {
            dd($error->getMessage());
        }
    }

    public function disconnect(Request $request, $id)
    {

        try {
            $response['message'] = "Aluno desligado com sucesso!";
            $response['success'] = true;
            $params = $request->all();

            if (!isset($id)) {
                return response([
                    'error' => true,
                    'error-msg' => 'ID do aluno não existe!'
                ], 404);
            }

            $aluno = Aluno::find($id);

            if (!$aluno) {
                return response([
                    'error' => true,
                    'error-msg' => 'Página não encontrada!'
                ], 404);
            }

            $aluno->data_desligamento = $params['data_desligamento'];
            $aluno->motivo_desligamento = $params['motivo_desligamento'];

            $aluno->save();
        } catch (\Exception $error) {
            $response['message'] = "Falha ao tentar desligar aluno. Mensagem do erro: <br>" . $error->getMessage();
            $response['success'] = false;
        }

        return json_encode($response);
    }
}
