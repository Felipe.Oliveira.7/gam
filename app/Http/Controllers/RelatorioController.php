<?php

namespace App\Http\Controllers;

use App\Models\Aluno;
use Illuminate\Http\Request;
use Exception;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;

class RelatorioController extends Controller
{

    // Total de itens da paginação
    const TOTAL_PAGINATION = 10;

    public function index()
    {
        return view('relatorios');
    }

    public function get(Request $request)
    {
        try {
            $params = $request->all();

            $perido_de = $params['periodo_de'] ?? false;
            $periodo_ate = $params['periodo_ate'] ?? false;
            $status = $params['status'] ?? false;
            $projeto = $params['projeto'] ?? false;
            $encaminhamento = $params['encaminhamento'] ?? false;
            $bairro = $params['bairro'] ?? false;
            $escola = $params['escola'] ?? false;
            $idade_de = $params['idade_de'] ?? false;
            $idade_ate = $params['idade_ate'] ?? false;
            $ordenar = $params['ordenar'];
            $em_ordem = $params['em_ordem'];

            $alunos = Aluno::query();

            if($perido_de){
                $alunos->where('data_inicio', '>=', $perido_de);
            }

            if ($periodo_ate) {
                $alunos->where('data_inicio', '<=', $periodo_ate);
            }

            if ($status) {
                if($status == 'ativo') {
                    $alunos->whereNull('data_desligamento');
                }
                if ($status == 'desligado') {
                    $alunos->whereNotNull('data_desligamento');
                }
            }

            if ($projeto) {
                $alunos->where('projeto', $projeto);
            }

            if ($encaminhamento) {
                $alunos->where('encaminhamento', $encaminhamento);
            }

            if ($bairro) {
                $alunos->where('bairro', $bairro);
            }

            if ($escola) {
                $alunos->where('escola', $escola);
            }

            if ($idade_de) {

                $dia_atual = Carbon::now()->format('d');
                $mes_atual = Carbon::now()->format('m');

                $ano_idade_de = ((int) Carbon::now()->format('Y')) - $idade_de;
                $data_idade_de = (string) $ano_idade_de . '-' . $mes_atual . '-' . $dia_atual;

                $alunos->where('data_nascimento', '<=', $data_idade_de);
            }

            if ($idade_ate) {
                $ano_idade_ate = ((int) Carbon::now()->format('Y')) - $idade_ate -1;
                $data_idade_ate = (string) $ano_idade_ate . '-01-01';

                $alunos->where('data_nascimento', '>=', $data_idade_ate);
            }

            if($ordenar == 'data_nascimento') {
                $em_ordem = ($em_ordem == 'asc') ? 'desc' : 'asc';
            }

            $alunos = $alunos->orderBy($ordenar,$em_ordem)->get();

            $total_alunos = count($alunos);

            return view('relatorios', ['alunos' => $alunos, 'params' => $params, 'total_alunos' => $total_alunos]);

        } catch(Exception $error) {
            $response['message'] = "Falha ao tentar gerar relatório de aluno. Mensagem do erro: <br>" . $error->getMessage();
            $response['success'] = false;
        }
    }

    public function pdf(Request $request)
    {
        try {
            $params = $request->all();
            $alunos = collect(json_decode($params['alunos']));
            parse_str($params['filtros'], $filtros);
            $total_alunos = count($alunos);

            view()->share('relatorio', ['alunos' => $alunos, 'filtros' => $filtros, 'total_alunos' => $total_alunos]);
            $pdf = Pdf::loadView('relatorio', ['alunos' => $alunos, 'filtros' => $filtros, 'total_alunos' => $total_alunos]);

            return $pdf->stream();

        } catch(Exception $error) {
            $response['message'] = "Falha ao tentar gerar PDF. Mensagem do erro: <br>" . $error->getMessage();
            $response['success'] = false;
        }
    }
}
