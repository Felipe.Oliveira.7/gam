<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Aluno extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nome',
        'serie',
        'escola',
        'data_nascimento',
        'endereco',
        'bairro',
        'numero',
        'telefone',
        'nis',
        'telefone_pai',
        'telefone_mae',
        'local_trabalho_pai',
        'local_trabalho_mae',
        'qtd_familiares',
        'renda_familiar',
        'nome_pai',
        'nome_mae',
        'data_inicio',
        'projeto',
        'encaminhamento',
        'data_desligamento',
        'motivo_desligamento',
        'observacao',
        'foto',
        'foto_tipo',
        'foto_nome',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'created_at' => 'datetime:d/m/Y H:00',
        'updated_at' => 'datetime:d/m/Y H:00',
        'data_nascimento' => 'date:Y-m-d',
        'data_inicio' => 'date:Y-m-d',
        'data_desligamento' => 'date:Y-m-d',
    ];

    public function getCreatedAtAttribute($date)
    {
        return $this->attributes['date'] = Carbon::parse($date)->format('d/m/Y H:i');
    }

    public function getDataNascimentoAttribute($date)
    {
        return $this->attributes['date'] = Carbon::parse($date)->format('Y-m-d');
    }

    public function getDataInicioAttribute($date)
    {
        return $this->attributes['date'] = Carbon::parse($date)->format('Y-m-d');
    }

    public function getDataDesligamentoAttribute($date)
    {
        return $this->attributes['date'] = ($date) ? Carbon::parse($date)->format('Y-m-d') : null;
    }
}
