<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Ocorrencia extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id_aluno',
        'data_ocorrencia',
        'educador',
        'tipo',
        'observacao',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'created_at' => 'datetime:d/m/Y H:00',
        'updated_at' => 'datetime:d/m/Y H:00',
        'data_ocorrencia' => 'date:Y-m-d'
    ];

    public function getCreatedAtAttribute($date)
    {
        return $this->attributes['date'] = Carbon::parse($date)->format('d/m/Y H:i');
    }

    public function getDataOcorrenciaAttribute($date)
    {
        return $this->attributes['date'] = Carbon::parse($date);
    }
}
